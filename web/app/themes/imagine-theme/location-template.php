<?php
/**
 * Template Name: Location Page
 */
?>


<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); ?>
	<div class="swiper-container">
		<div class="swiper-wrapper">

			<div class="swiper-slide">

				<div class="section-content location-section">
                    <div class="bg" style="background-image: url('<?= get_template_directory_uri();?>/dist/images/location-top-bg.jpg')"></div>

                    <div class="infoboxes-container">

						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
                        <svg class="svg-box _long" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 423.6 208.6">
                            <rect width="423.6" height="208.6" fill="#1ABFD5"/>
                            <defs>
                                <rect id="a" width="441.6" height="222.9" x="-9" y="-7"/>
                            </defs>
                            <clipPath id="b">
                                <use xlink:href="#a" overflow="visible"/>
                            </clipPath>
                            <path fill="#FFF" d="M212.4 110.8c-2.6-1-5.5.2-6.5 2.6l-1.7 4.5c-.6 1.8-.2 3.9 1.4 5.4 1.9 1.8 6.1 2.1 7.2-.3.7-1.5 2.5-5.9 2.5-5.9 1-2.5-.3-5.3-2.9-6.3m-7.4-7.4c-2.4-1.4-5.4-.7-6.8 1.5l-3.2 5.2c-1 1.8-.8 4.1.6 5.8 1.8 2.1 5.5 2.5 6.9.7 1.4-1.8 4.3-6.6 4.3-6.6 1.5-2.2.6-5.2-1.8-6.6m-6.6-7.9c-2.3-1.6-5.4-1.2-6.9.9l-4.6 6.1c-1.7 1.9-1.5 4.9.5 6.8 2 1.9 5.2 1.9 7 0l5.3-7c1.6-2.1 1-5.2-1.3-6.8m-7.3-6.5c-2-1.9-5.2-1.9-7 0l-3.7 4c-1.8 1.9-1.6 5 .4 7 1.9 1.8 4.7 1.9 6.5.4.5-.6 4.2-4.4 4.2-4.4 1.8-2 1.7-5.1-.4-7M247 47.6l-35.6-3.2-26.9 18v.7s0 .1.1.1c.1.1.1.2.2.2l.1.1.1.1.1.1.1.1.1.1.1.1c.1.2.3.3.5.5l.1.1.1.1.1.1c.1 0 .1.1.2.1s.1.1.2.1c.2.2.4.3.7.4.1 0 .1.1.2.1h.1c.1 0 .1.1.2.1h.1c.1 0 .1.1.2.1h.1c.1 0 .1.1.2.1h.1c.1 0 .1.1.2.1h.1l.9.3h.2c.1 0 .2 0 .3.1h.2c.3.1.5.1.8.1.4 0 .9.1 1.3.1h.4c.4 0 .7-.1 1.1-.1.3 0 .6-.1.9-.2.5-.1 1.1-.3 1.7-.5h.2c.1 0 .3-.1.4-.1 0 0 .1 0 .1-.1h.1s.1 0 .1-.1c.1 0 .1-.1.2-.1 0 0 .1 0 .1-.1.1 0 .1-.1.2-.1s.2-.1.2-.1h.1c.1 0 .1-.1.2-.1 0 0 .1 0 .1-.1.1 0 .1-.1.2-.1s.1-.1.2-.1c0 0 .1 0 .1-.1.1 0 .1-.1.2-.1 4.1-2.1 11.2-5.3 11.2-5.3l1.2 1c2.5 2.1 9.1 7.4 16.9 10.3l.6.2 3.1 3.4 14.2 15 6.3-5.1 1.7-.2-7.5-36.1z" clip-path="url(#b)"/>
                            <path fill="#FFF" d="M244.7 90.8l-13.5-14.4-2.7-2.9c-8.2-3-15-8.4-17.7-10.7-3.6 1.6-9.9 4.6-11.6 5.7l-.2.1-.2.1c-2.1.8-4.1 1.2-6 1.2-7.5 0-10.4-6.1-10.5-6.4l-.9-2L205 45.6l-28.5 2.3-7.5 35.8 1.3.2 10.6 5.7 1.7-1.8c1.2-1.3 3-2.1 4.8-2.1 1.8 0 3.5.7 4.8 1.9 1.3 1.3 2.1 2.9 2.2 4.7v.2h.7c1.5 0 3 .5 4.2 1.4 1.5 1.1 2.5 2.7 2.8 4.4.1.8.1 1.6 0 2.4 1.3 0 2.5.4 3.6 1 1.6.9 2.7 2.4 3.2 4.2.3.9.3 1.8.2 2.7.3 0 .7-.1 1-.1.9 0 1.8.2 2.6.5 1.7.7 3.1 1.9 3.9 3.6.8 1.7.9 3.5.2 5.2v.1l-1.9 4.4c1.5.2 3.3-.7 4.7-2 2-1.9 1.6-5.4.3-6.6l-9-9.5.7-.8 9.4 9.9c1.8 1.9 5 1.8 7.1-.2 2-1.9 2.3-4.6.5-6.5l-10.1-10.7.7-.8 10 10.5c1.8 1.9 5.2 1.9 7.2 0s2-4.9.2-6.8l-10.5-11.2.7-.7 10.3 10.9c1.8 1.9 4.9 1.9 6.9 0s2.5-5 .7-7m-92.1-10.7c-1.5 0-2.7-1.2-2.7-2.7 0-1.5 1.2-2.7 2.7-2.7 1.5 0 2.7 1.2 2.7 2.7 0 1.5-1.2 2.7-2.7 2.7m17.8-39.5l-20.5-4.3c-.3-.1-.5-.1-.8-.1l-9.6 46.1c.2.1.5.2.7.2l20.5 4.3c2 .4 4.1-.9 4.5-2.9l8.1-38.8c.4-2.1-.9-4.1-2.9-4.5m100.3 40.1c-1.5 0-2.7-1.2-2.7-2.7s1.2-2.7 2.7-2.7c1.5 0 2.7 1.2 2.7 2.7s-1.3 2.7-2.7 2.7m3.9-44.5c-.3 0-.5 0-.8.1l-20.5 4.3c-2 .4-3.4 2.4-2.9 4.5l8.1 38.8c.4 2 2.4 3.4 4.5 2.9l20.5-4.3c.3-.1.5-.1.7-.2l-9.6-46.1z" clip-path="url(#b)"/>
                            <text transform="translate(65.72 175.758)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">M</tspan><tspan x="19.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="25.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan><tspan x="37.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">J</tspan><tspan x="46.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">S</tspan><tspan x="57.9" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">C</tspan><tspan x="71.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E W </tspan><tspan x="115.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">R</tspan><tspan x="129.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan><tspan x="144.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="160.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">K</tspan><tspan x="174.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="180.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="196.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">G</tspan><tspan x="211.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">U </tspan><tspan x="232.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">M</tspan><tspan x="251.9" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="257.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan><tspan x="272.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">S</tspan><tspan x="284.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">T</tspan>
                            </text>
                            <text transform="translate(65.72 198.754)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">P</tspan><tspan x="12.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">R</tspan><tspan x="26.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Z</tspan><tspan x="39.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21" letter-spacing="-1">Y</tspan><tspan x="51.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">J</tspan><tspan x="59.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan><tspan x="74.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Z</tspan><tspan x="86.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="102.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21" letter-spacing="-1">Y</tspan><tspan x="115.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">C</tspan><tspan x="128.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">H </tspan><tspan x="150.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">B</tspan><tspan x="163.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="169.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Z</tspan><tspan x="181.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="197.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan><tspan x="209.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">S</tspan><tspan x="221.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">O</tspan><tspan x="237.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">W</tspan><tspan x="259.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan>
                            </text>
                            <text fill="#FFF" transform="translate(11.656 199.46)" font-family="'Lato-Bold'" font-size="80">
                                5
                            </text>
                        </svg>
                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>


					</div>
					<div class="section-copy">
						<h1 class="heading">Idealna Lokalizacja</h1>
						<p class="copy">
                            Nieograniczone możliwości i perspektywy dla biznesu, duży potencjał dla rozwoju, pionier rewitalizacji – wyobraź sobie swój biznes właśnie tutaj, w Łodzi. Budynki Imagine znajdują się nie tylko w samym sercu Polski, niedaleko Warszawy, ale mają także wyjątkową lokalizację w obrębie samej Łodzi – w pobliżu głównego węzła komunikacyjnego, z doskonałym dostępem do transportu publicznego.					</div>
				</div>

			</div>
            <div class="swiper-slide">
				<div class="section-content googlemap-section">
                    <button id="map-handler" class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 554.625 554.625">
                            <g fill="#1fbed8">
                                <path d="M154.142,86.062c0-36.337,30.6-66.938,66.938-66.938s66.937,30.6,66.937,66.938v53.55    c11.476-15.3,19.125-34.425,19.125-53.55C307.141,38.25,268.891,0,221.079,0c-47.812,0-86.062,38.25-86.062,86.062    c0,21.038,7.65,40.163,19.125,53.55V86.062z"/>
                                <path d="M450.579,229.5c-11.475,0-21.037,3.825-28.688,9.562l0,0c0-26.775-21.037-47.812-47.812-47.812    c-11.475,0-22.949,3.825-32.512,11.475c-7.65-19.125-24.863-30.6-43.988-30.6c-11.475,0-21.037,3.825-28.688,9.562V86.062    c0-26.775-21.037-47.812-47.812-47.812s-47.812,21.038-47.812,47.812v170.212c-38.25-34.425-80.325-61.2-107.1-34.425    c-38.25,38.25,42.075,112.837,103.275,225.675c43.988,78.412,105.188,107.1,166.387,107.1    c89.888,0,162.562-72.675,162.562-162.562v-114.75C498.391,250.538,477.354,229.5,450.579,229.5z M479.266,319.388v72.675    c0,74.588-65.024,143.438-143.438,143.438c-72.675,0-114.75-40.162-149.175-95.625c-72.675-126.225-126.225-183.6-107.1-204.638    c21.037-21.037,72.675,24.863,112.837,66.938V86.062c0-15.3,13.387-28.688,28.688-28.688c15.3,0,28.688,13.388,28.688,28.688    v200.812h19.125v-66.938c0-15.3,13.388-28.688,28.688-28.688c15.301,0,28.688,13.388,28.688,28.688v47.812h19.125v-28.688    c0-15.3,13.388-28.688,28.688-28.688c15.301,0,28.688,13.388,28.688,28.688v47.812h19.125v-9.562    c0-15.3,13.388-28.688,28.688-28.688c15.301,0,28.688,13.388,28.688,28.688V319.388z"/>
                            </g>
                        </svg>                    </button>
                    <div class="map-overlay">
                    </div>
                    <div id="location-map" class="swiper-no-swiping">



                    </div>




                </div>
			</div>
			<div class="swiper-slide">
				<div class="section-content infrastructure-section">
                    <div class="bg" style="background-image: url('<?= get_template_directory_uri();?>/dist/images/infrastructureBG.jpg')"></div>

                    <div class="infoboxes-container">

                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 208.6 423.6">
                            <path fill="#79BC43" d="M0 0h208.6v423.6H0z"/>
                            <path fill="#FFF" d="M107 204.7v-91.8l13.6 13.7 3.9-3.8-20.2-20.2-20.1 20.2 3.8 3.8 13.6-13.7v91.8L88 191l-3.8 3.9 20.1 20.1 20.2-20.1-3.9-3.9"/>
                            <path fill="#FFF" d="M58.5 161.5h91.7l-13.7 13.6 3.9 3.9 20.1-20.2-20.1-20.1-3.9 3.8 13.7 13.6H58.5l13.6-13.6-3.8-3.8-20.2 20.1L68.3 179l3.8-3.9"/>
                            <text transform="translate(10.69 336.23)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">N</tspan><tspan x="14.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">A </tspan><tspan x="32.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">S</tspan><tspan x="42.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">K</tspan><tspan x="55.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">R</tspan><tspan x="67.9" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">Z</tspan><tspan x="79.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">Y</tspan><tspan x="92.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">Ż</tspan><tspan x="103" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">O</tspan><tspan x="117.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19" letter-spacing="-1">W</tspan><tspan x="136.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">A</tspan><tspan x="149.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">N</tspan><tspan x="164.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">I</tspan><tspan x="169.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">U</tspan>
                            </text>
                            <text transform="translate(10.69 358.23)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">K</tspan><tspan x="12.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">R</tspan><tspan x="25.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">A</tspan><tspan x="39" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">J</tspan><tspan x="47.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">O</tspan><tspan x="61.9" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">W</tspan><tspan x="82.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">Y</tspan><tspan x="93.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">C</tspan><tspan x="106" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">H </tspan><tspan x="125.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">D</tspan><tspan x="139.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">R</tspan><tspan x="151.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">Ó</tspan><tspan x="166.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">G</tspan>
                            </text>
                            <text transform="translate(10.69 380.233)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">W</tspan><tspan x="19.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">S</tspan><tspan x="30" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">C</tspan><tspan x="42.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">HÓ</tspan><tspan x="72.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">D</tspan><tspan x="86.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19" letter-spacing="-1">-</tspan><tspan x="92.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">Z</tspan><tspan x="104.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">A</tspan><tspan x="117" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">C</tspan><tspan x="129.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">HÓ</tspan><tspan x="159.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">D</tspan>
                            </text>
                            <text transform="translate(10.69 403.24)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">P</tspan><tspan x="11.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">Ó</tspan><tspan x="27.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">Ł</tspan><tspan x="37" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">N</tspan><tspan x="51.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">O</tspan><tspan x="66.9" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19" letter-spacing="-1">C</tspan><tspan x="78.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">-</tspan><tspan x="85.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">P</tspan><tspan x="97.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">O</tspan><tspan x="112.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">Ł</tspan><tspan x="122.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">U</tspan><tspan x="136.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">D</tspan><tspan x="150.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">NI</tspan><tspan x="170.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="19">E</tspan>
                            </text>
                            <text fill="#FFF" transform="translate(191.494 403.23)" font-family="'Lato-Bold'" font-size="21">

                            </text>
                        </svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
                        <svg class="svg-box _hr-rectangle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 423.6 208.6">
                            <rect width="423.6" height="208.6" fill="#1ABFD5"></rect>
                            <defs>
                                <rect id="a" width="441.6" height="222.9" x="-9" y="-7"></rect>
                            </defs>
                            <clipPath id="b">
                                <use xlink:href="#a" overflow="visible"></use>
                            </clipPath>
                            <path fill="#FFF" d="M212.4 110.8c-2.6-1-5.5.2-6.5 2.6l-1.7 4.5c-.6 1.8-.2 3.9 1.4 5.4 1.9 1.8 6.1 2.1 7.2-.3.7-1.5 2.5-5.9 2.5-5.9 1-2.5-.3-5.3-2.9-6.3m-7.4-7.4c-2.4-1.4-5.4-.7-6.8 1.5l-3.2 5.2c-1 1.8-.8 4.1.6 5.8 1.8 2.1 5.5 2.5 6.9.7 1.4-1.8 4.3-6.6 4.3-6.6 1.5-2.2.6-5.2-1.8-6.6m-6.6-7.9c-2.3-1.6-5.4-1.2-6.9.9l-4.6 6.1c-1.7 1.9-1.5 4.9.5 6.8 2 1.9 5.2 1.9 7 0l5.3-7c1.6-2.1 1-5.2-1.3-6.8m-7.3-6.5c-2-1.9-5.2-1.9-7 0l-3.7 4c-1.8 1.9-1.6 5 .4 7 1.9 1.8 4.7 1.9 6.5.4.5-.6 4.2-4.4 4.2-4.4 1.8-2 1.7-5.1-.4-7M247 47.6l-35.6-3.2-26.9 18v.7s0 .1.1.1c.1.1.1.2.2.2l.1.1.1.1.1.1.1.1.1.1.1.1c.1.2.3.3.5.5l.1.1.1.1.1.1c.1 0 .1.1.2.1s.1.1.2.1c.2.2.4.3.7.4.1 0 .1.1.2.1h.1c.1 0 .1.1.2.1h.1c.1 0 .1.1.2.1h.1c.1 0 .1.1.2.1h.1c.1 0 .1.1.2.1h.1l.9.3h.2c.1 0 .2 0 .3.1h.2c.3.1.5.1.8.1.4 0 .9.1 1.3.1h.4c.4 0 .7-.1 1.1-.1.3 0 .6-.1.9-.2.5-.1 1.1-.3 1.7-.5h.2c.1 0 .3-.1.4-.1 0 0 .1 0 .1-.1h.1s.1 0 .1-.1c.1 0 .1-.1.2-.1 0 0 .1 0 .1-.1.1 0 .1-.1.2-.1s.2-.1.2-.1h.1c.1 0 .1-.1.2-.1 0 0 .1 0 .1-.1.1 0 .1-.1.2-.1s.1-.1.2-.1c0 0 .1 0 .1-.1.1 0 .1-.1.2-.1 4.1-2.1 11.2-5.3 11.2-5.3l1.2 1c2.5 2.1 9.1 7.4 16.9 10.3l.6.2 3.1 3.4 14.2 15 6.3-5.1 1.7-.2-7.5-36.1z" clip-path="url(#b)"></path>
                            <path fill="#FFF" d="M244.7 90.8l-13.5-14.4-2.7-2.9c-8.2-3-15-8.4-17.7-10.7-3.6 1.6-9.9 4.6-11.6 5.7l-.2.1-.2.1c-2.1.8-4.1 1.2-6 1.2-7.5 0-10.4-6.1-10.5-6.4l-.9-2L205 45.6l-28.5 2.3-7.5 35.8 1.3.2 10.6 5.7 1.7-1.8c1.2-1.3 3-2.1 4.8-2.1 1.8 0 3.5.7 4.8 1.9 1.3 1.3 2.1 2.9 2.2 4.7v.2h.7c1.5 0 3 .5 4.2 1.4 1.5 1.1 2.5 2.7 2.8 4.4.1.8.1 1.6 0 2.4 1.3 0 2.5.4 3.6 1 1.6.9 2.7 2.4 3.2 4.2.3.9.3 1.8.2 2.7.3 0 .7-.1 1-.1.9 0 1.8.2 2.6.5 1.7.7 3.1 1.9 3.9 3.6.8 1.7.9 3.5.2 5.2v.1l-1.9 4.4c1.5.2 3.3-.7 4.7-2 2-1.9 1.6-5.4.3-6.6l-9-9.5.7-.8 9.4 9.9c1.8 1.9 5 1.8 7.1-.2 2-1.9 2.3-4.6.5-6.5l-10.1-10.7.7-.8 10 10.5c1.8 1.9 5.2 1.9 7.2 0s2-4.9.2-6.8l-10.5-11.2.7-.7 10.3 10.9c1.8 1.9 4.9 1.9 6.9 0s2.5-5 .7-7m-92.1-10.7c-1.5 0-2.7-1.2-2.7-2.7 0-1.5 1.2-2.7 2.7-2.7 1.5 0 2.7 1.2 2.7 2.7 0 1.5-1.2 2.7-2.7 2.7m17.8-39.5l-20.5-4.3c-.3-.1-.5-.1-.8-.1l-9.6 46.1c.2.1.5.2.7.2l20.5 4.3c2 .4 4.1-.9 4.5-2.9l8.1-38.8c.4-2.1-.9-4.1-2.9-4.5m100.3 40.1c-1.5 0-2.7-1.2-2.7-2.7s1.2-2.7 2.7-2.7c1.5 0 2.7 1.2 2.7 2.7s-1.3 2.7-2.7 2.7m3.9-44.5c-.3 0-.5 0-.8.1l-20.5 4.3c-2 .4-3.4 2.4-2.9 4.5l8.1 38.8c.4 2 2.4 3.4 4.5 2.9l20.5-4.3c.3-.1.5-.1.7-.2l-9.6-46.1z" clip-path="url(#b)"></path>
                            <text transform="translate(65.72 175.758)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">M</tspan><tspan x="19.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="25.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan><tspan x="37.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">J</tspan><tspan x="46.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">S</tspan><tspan x="57.9" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">C</tspan><tspan x="71.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E W </tspan><tspan x="115.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">R</tspan><tspan x="129.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan><tspan x="144.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="160.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">K</tspan><tspan x="174.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="180.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="196.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">G</tspan><tspan x="211.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">U </tspan><tspan x="232.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">M</tspan><tspan x="251.9" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="257.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan><tspan x="272.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">S</tspan><tspan x="284.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">T</tspan>
                            </text>
                            <text transform="translate(65.72 198.754)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">P</tspan><tspan x="12.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">R</tspan><tspan x="26.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Z</tspan><tspan x="39.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21" letter-spacing="-1">Y</tspan><tspan x="51.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">J</tspan><tspan x="59.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan><tspan x="74.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Z</tspan><tspan x="86.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="102.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21" letter-spacing="-1">Y</tspan><tspan x="115.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">C</tspan><tspan x="128.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">H </tspan><tspan x="150.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">B</tspan><tspan x="163.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="169.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Z</tspan><tspan x="181.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="197.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan><tspan x="209.8" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">S</tspan><tspan x="221.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">O</tspan><tspan x="237.5" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">W</tspan><tspan x="259.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan>
                            </text>
                            <text fill="#FFF" transform="translate(11.656 199.46)" font-family="'Lato-Bold'" font-size="80">
                                5
                            </text>
                        </svg>
                        <svg class="svg-box _nd-placegolder" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box _nd-placegolder" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>
						<svg class="svg-box last" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>


					</div>


				</div>
			</div>
			<div class="swiper-slide">
                <div class="section-content visualization-section">
                    <div class="bg" style="background-image: url('<?= get_template_directory_uri();?>/dist/images/visualizationBG.jpg')"></div>






                </div>
		</div>
		<!-- Add Pagination -->


<?php endwhile; ?>