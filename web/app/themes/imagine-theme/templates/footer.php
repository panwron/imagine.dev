<footer class="content-info">
  <div class="container-fluid">
    <?php dynamic_sidebar('sidebar-footer'); ?>
      
      <div class="row">
          <div class="col-lg-2 col-xs-3 footer-left-col">
              <a class="brand-logo" href="<?= esc_url(home_url('/')); ?>">
                  <img class="footer-logo" src="<?= get_template_directory_uri();?>/dist/images/imagine-logo-full.png" alt="">
              </a>
          </div>
          <div class="col-lg-8 col-xs-6 footer-center-col">
              <p>kontakt w sprawie najmu <span class="phone-num">22 31 78 00</span> </p>
          </div>

          <div class="col-lg-2 col-xs-3 footer-right-col">
              <img src="<?= get_template_directory_uri();?>/dist/images/colliers-logo.jpg" alt="">

          </div>

          

      </div>
  </div>
</footer>

<script>
    function initMap() {
        var uluru = {lat: 51.760885, lng: 19.486880};
        var map = new google.maps.Map(document.getElementById('location-map'), {
            zoom: 16,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map,
            icon: wp.theme_url + "/dist/images/imagine-map-ico.png"
        });
    }
</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpV_9wmIJEz08Lb9c6_6R4nVm_lxDRJ_M&callback=initMap">
</script>
