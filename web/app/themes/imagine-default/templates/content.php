<article <?php post_class(); ?>>
<!--    <h2 class="entry-title"><a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a></h2>-->

    <a href="<?php the_permalink(); ?>">

<!--    <div class="post-date">--><?php //get_template_part('templates/entry-meta'); ?><!--</div>-->

  <div class="entry-summary">
      <p class="excerpt">
          <time class="post-date"> <?= get_the_date('d.m.Y'); ?></time>
          <?php echo wp_strip_all_tags( get_the_excerpt(), true ); ?>
      </p>
  </div>
    </a>
</article>
