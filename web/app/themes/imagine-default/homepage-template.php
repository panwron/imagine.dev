<?php
/**
 * Template Name: Homepage Page
 */
?>


<?php while ( have_posts() ) : the_post(); ?>
	<?php the_content(); ?>

    <div class="front-img" style="background-image: url('<?php the_field("bg"); ?>')">


    </div>
    <div class="welcome-txt">
        <svg xmlns="http://www.w3.org/2000/svg" data-name="Warstwa 1" viewBox="0 0 175.97 167.31">
            <text fill="#00b188" transform="translate(.23 31.01)" font-size="36" font-family="Lato" font-weight="700">
                I<tspan x="11.47" y="0">M</tspan><tspan x="46.46" y="0">A</tspan><tspan x="71.46" y="0" letter-spacing="1">G</tspan><tspan x="98.64" y="0">I</tspan><tspan x="110.14" y="0" letter-spacing="1">N</tspan><tspan x="138.99" y="0">E</tspan>
            </text>
            <text fill="#79bc43" transform="translate(.23 73.03)" font-size="36" font-family="Lato" font-weight="700">
                Y<tspan x="22.64" y="0" letter-spacing="1">O</tspan><tspan x="52.81" y="0" letter-spacing="1">U</tspan><tspan x="80.69" y="0">R</tspan>
            </text>
            <text fill="#1abfd5" transform="translate(.23 115.04)" font-size="36" font-family="Lato" font-weight="700">
                B<tspan x="24.68" y="0">U</tspan><tspan x="52.27" y="0">S</tspan><tspan x="72.95" y="0">I</tspan><tspan x="84.46" y="0" letter-spacing="1">N</tspan><tspan x="113.31" y="0" letter-spacing="1">E</tspan><tspan x="135.27" y="0">S</tspan><tspan x="155.99" y="0">S</tspan>
            </text>
            <text fill="#00b188" transform="translate(.23 157.05)" font-size="36" font-family="Lato" font-weight="700">
                I<tspan x="11.5" y="0" letter-spacing="1">N </tspan><tspan x="50.8" y="0">Ł</tspan><tspan x="70.11" y="0" letter-spacing="1">Ó</tspan><tspan x="100.35" y="0">DŹ</tspan>
            </text>
        </svg>
    </div>

<?php endwhile; ?>