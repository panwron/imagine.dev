<?php get_template_part('templates/page', 'header'); ?>

<?php if (!have_posts()) : ?>
	<div class="alert alert-warning">
		<?php _e('Sorry, no results were found.', 'sage'); ?>
	</div>
	<?php get_search_form(); ?>
<?php endif; ?>


<div class="posts-container">

	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	<?php endwhile; ?>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4">
        <?php the_posts_pagination( array(
            'screen_reader_text' => __( ' ', '' ),
            'prev_text' => __( '<', 'textdomain' ),
            'next_text' => __( '>', 'textdomain' ),
        ) ); ?>
    </div>
    <div class="col-sm-4">
        <a href="#" class="brochure-download">
            <?php pll_e('pobierz broszurę'); ?>
        </a>
    </div>
</div>

</div>

<?php get_template_part('templates/gallery'); ?>
