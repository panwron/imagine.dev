<?php
/**
 * Template Name: Location Page
 */
?>


<?php while (have_posts()) : the_post(); ?>

				<div class="section-content location-section">

                    <div class="bg" style="background-image: url('<?= the_field('s1-bg');?>')"></div>
                    <div class="infoboxes-container">

                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Warstwa 1" viewBox="0 0 208.63 208.63">
                            <defs>
                                <clipPath id="a">
                                    <path fill="none" d="M0 0h226.61v208.63H0z"/>
                                </clipPath>
                            </defs>
                            <path fill="#00b188" d="M0 0h208.63v208.63H0z"/>
                            <text fill="#fff" transform="translate(9.81 175.76)" font-size="21" font-family="Lato" font-weight="700">
                                S<tspan x="11.48" y="0">P</tspan><tspan x="24.01" y="0">E</tspan><tspan x="35.48" y="0">C</tspan><tspan x="49.41" y="0">J</tspan><tspan x="57.65" y="0">A</tspan><tspan x="72.11" y="0">L</tspan><tspan x="82.69" y="0">N</tspan><tspan x="98.72" y="0">A </tspan><tspan x="118.41" y="0">S</tspan><tspan x="129.97" y="0">T</tspan><tspan x="142.27" y="0">RE</tspan><tspan x="167.35" y="0" letter-spacing="-1">F</tspan><tspan x="177.83" y="0">A</tspan>
                            </text>
                            <text fill="#fff" transform="translate(9.81 198.75)" font-size="21" font-family="Lato" font-weight="700">
                                E<tspan x="11.78" y="0">K</tspan><tspan x="25.19" y="0">ONO</tspan><tspan x="75.12" y="0">M</tspan><tspan x="94.73" y="0">I</tspan><tspan x="100.81" y="0">C</tspan><tspan x="114.62" y="0">Z</tspan><tspan x="126.98" y="0">N</tspan><tspan x="143.01" y="0">A</tspan>
                            </text>
                            <path fill="#fff" d="M118.91 82.4h10.63v28.54h-10.63z"/>
                            <g clip-path="url(#a)">
                                <path fill="#fff" d="M135.29 64.88C113.62 86.29 72.4 92.12 72.4 92.12v-5s29.12-8.46 44.41-31.91h-13L135.29 35l8.93 35.69z"/>
                            </g>
                            <path fill="#fff" d="M98.1 92.12h10.65v18.82H98.1zm-20.16 6.94h10.63v11.88H77.94z"/>
                            <g clip-path="url(#a)">
                                <path fill="#fff" d="M103.43 124.75c-27.24 0-54.82-6.32-54.82-18.4 0-5.93 6.55-10.85 18.94-14.23l1.53 5.81c-10 2.73-14.56 6.28-14.56 8.42 0 5 19.05 12.38 48.91 12.38s48.91-7.33 48.91-12.38c0-1.87-3.52-4.9-11.38-7.47l1.81-5.73c10.27 3.36 15.48 7.8 15.48 13.2 0 12.08-27.58 18.4-54.82 18.4"/>
                            </g>
                        </svg>
                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Warstwa 1" viewBox="0 0 208.63 208.63">
                            <path fill="#79bc43" d="M0 0h208.63v208.63H0z"/>
                            <text fill="#fff" transform="translate(10.18 172.94)" font-size="85" font-family="Lato">
                                3
                            </text>
                            <text fill="#fff" transform="translate(67.75 129.25)" font-size="21" font-family="Lato" font-weight="700">
                                C<tspan x="13.22" y="0">O </tspan><tspan x="35.29" y="0">D</tspan><tspan x="51.22" y="0">O</tspan>
                            </text>
                            <text fill="#fff" transform="translate(67.75 152.25)" font-size="21" font-family="Lato" font-weight="700">
                                W<tspan x="21.71" y="0">IE</tspan><tspan x="39.51" y="0">L</tspan><tspan x="50.04" y="0">K</tspan><tspan x="63.45" y="0">O</tspan><tspan x="80.21" y="0">Ś</tspan><tspan x="91.48" y="0">C</tspan><tspan x="104.86" y="0">I</tspan>
                            </text>
                            <text fill="#fff" transform="translate(67.75 175.24)" font-size="21" font-family="Lato" font-weight="700">
                                M<tspan x="19.61" y="0">I</tspan><tspan x="25.5" y="0">A</tspan><tspan x="40.24" y="0">S</tspan><tspan x="51.8" y="0">T</tspan><tspan x="63.74" y="0">O</tspan>
                            </text>
                            <text fill="#fff" transform="translate(152.35 175.24)" font-size="21" font-family="Lato" font-weight="700">

                            </text>
                            <text fill="#fff" transform="translate(67.75 198.24)" font-size="21" font-family="Lato" font-weight="700">
                                W <tspan x="27.17" y="0">P</tspan><tspan x="40.05" y="0">O</tspan><tspan x="56.95" y="0">L</tspan><tspan x="68.09" y="0">S</tspan><tspan x="79.53" y="0">C</tspan><tspan x="93.04" y="0">E</tspan>
                            </text>
                            <path fill="#fff" d="M75.77 49h4.13v-2h-4.13v2zm0 2.89h4.13v-1.97h-4.13v1.93zm-11.46 0h4.13v-1.97H64.3v1.93zm5.37 0h1.46v-1.97h-1.47v1.93zm-1.24 1.27H64.3V55h4.13v-1.88zm11.47 0h-4.14V55h4.13v-1.88zm-5.37 0h-1.47V55h1.46v-1.88zm5.37 3.19h-10v1.93h10v-1.97zm0 2.89h-4.16v1.93h4.13v-1.98zm-5.37 0h-1.49v1.93h1.46v-1.98zm1.24 5.12h4.13v-1.97h-4.16v1.93zm-11.46 0h4.13v-1.97h-4.16v1.93zm5.37 0h1.46v-1.97h-1.49v1.93zm6.1 3.37h4.11v-1.98h-4.13v1.93zm-11.46 0h4.13v-1.98H64.3v1.93zm5.37 0h1.46v-1.98h-1.49v1.93zM68.46 69H64.3v1.93h4.13v-1.98zm11.47 0h-4.16v1.93h4.13v-1.98zm-5.37 0h-1.49v1.93h1.46v-1.98zm5.37 3.19h-10v1.93h10v-1.98zm-5.37 3.4h-1.49v1.93h1.46v-1.98zm2.51 0h-1.49v1.93H77v-1.98zm2.86 0h-1.49v1.93h1.46v-1.98zm-14.11 0h-1.49v1.93h1.46v-1.98zm2.51 0h-1.48v1.93h1.45v-1.98zm2.86 0h-1.48v1.93h1.46v-1.98zm8.73 3.29h-10v1.93h10v-1.98zm-5.37 3.4h-1.48v1.93h1.46v-1.99zm2.51 0h-1.48v1.93H77v-1.99zm2.86 0h-1.48v1.93h1.46v-1.99zm-14.11 0h-1.48v1.93h1.46v-1.99zm2.51 0h-1.47v1.93h1.45v-1.99zm2.86 0h-1.47v1.93h1.46v-1.99zm8.73 3.69h-10v1.93h10v-1.98zm39.94-14.55h-10v1.92h10v-1.97zm-11.48 0h-4.13v1.92h4.13v-1.97zm0-2.89h-4.13v1.87h4.13v-1.92zm11.46 0h-4.13v1.87h4.13v-1.92zm-5.37 0H113v1.87h1.46v-1.92zm1.24-1.27h4.13v-1.98h-4.13v1.93zm-11.46 0h4.13v-1.98h-4.13v1.93zm5.37 0h1.46v-1.98h-1.47v1.93zM104.23 64h10v-1.9h-10V64zm0-2.89h4.13v-1.9h-4.13v1.93zm5.37 0h1.46v-1.9h-1.46v1.93zM108.37 56h-4.13v1.93h4.13V56zm11.47 0h-4.13v1.93h4.13V56zm-5.37 0H113v1.93h1.46V56zm-6.1-3.37h-4.13v1.93h4.13v-1.91zm11.46 0h-4.13v1.93h4.13v-1.91zm-5.37 0H113v1.93h1.46v-1.91zm1.24-1.27h4.13v-1.91h-4.13v1.93zm-11.46 0h4.13v-1.91h-4.13v1.93zm5.37 0h1.46v-1.91h-1.47v1.93zm-5.37-3.19h10v-1.91h-10v1.93zm5.37-3.4h1.46v-1.91h-1.47v1.93zm-2.51 0h1.46v-1.91h-1.46v1.93zm-2.86 0h1.46v-1.91h-1.46v1.93zm14.11 0h1.46v-1.91h-1.46v1.93zm-2.51 0h1.46v-1.91h-1.46v1.93zm-2.86 0h1.46v-1.91H113v1.93zm-8.73-3.29h10v-1.91h-10v1.93zm5.37-3.4h1.46v-1.91h-1.48v1.93zm-2.51 0h1.46v-1.91h-1.46v1.93zm-2.86 0h1.46v-1.91h-1.46v1.93zm14.11 0h1.46v-1.91h-1.46v1.93zm-2.51 0h1.46v-1.91h-1.46v1.93zm-2.86 0h1.46v-1.91H113v1.93zM109.6 31h1.46v-1.92h-1.46V31zm-2.51 0h1.46v-1.92h-1.46V31zm-2.86 0h1.46v-1.92h-1.46V31zm14.11 0h1.46v-1.92h-1.46V31zm-2.51 0h1.46v-1.92h-1.46V31zM113 31h1.46v-1.92H113V31zm2.74 58.43h4.13V87.5h-4.17v1.93zm4.14-11.2h-4.13v1.93h4.13v-1.93zm-5.37 0H113v1.93h1.46v-1.93zm5.36-3.36h-4.17v1.93h4.13v-1.93zm-5.37 0H113v1.93h1.46v-1.93zm15.82-8.42h4.13v-1.93h-4.13v1.93zm5.37 0h1.46v-1.93h-1.46v1.93zm-5.37-3.19h10v-1.93h-10v1.93zm0-2.89h4.13v-1.93h-4.13v1.93zm5.37 0h1.46v-1.93h-1.46v1.93zm-1.24-5.12h-4.13v1.93h4.13v-1.93zm6.09 0h-1.46v1.93h1.46v-1.93zm-6.11-3.37h-4.13v1.93h4.13v-1.93zm6.09 0H139v1.93h1.46v-1.93zm-10.22-1.27h4.13v-1.92h-4.13v1.93zm5.37 0h1.46v-1.92h-1.46v1.93zm-5.37-3.19h10V45.5h-10v1.93zm5.37-3.4h1.46V42.1h-1.46V44zm-2.51 0h1.46V42.1h-1.46V44zm-2.86 0h1.46V42.1h-1.46V44zM139 44h1.46v-1.9H139V44zm-8.73-3.29h10v-1.9h-10v1.93zm5.37-3.4h1.46v-1.9h-1.46v1.93zm-2.51 0h1.46v-1.9h-1.46v1.93zm-2.86 0h1.46v-1.9h-1.46v1.93zm8.74 0h1.46v-1.9H139v1.93zm-3.36-7.09h1.46v-1.9h-1.46v1.93zm-2.51 0h1.46v-1.9h-1.46v1.93zm-2.86 0h1.46v-1.9h-1.46v1.93zm8.74 0h1.46v-1.9H139v1.93zM97.37 82.69h1.46v-1.92h-1.46v1.93zm-2.51 0h1.46v-1.92h-1.47v1.93zm-2.86 0h1.46v-1.92H92v1.93zM97.37 76h1.46v-1.92h-1.46V76zm-2.51 0h1.46v-1.92h-1.47V76zM92 76h1.46v-1.92H92V76zm5.38-7.09h1.46V67h-1.47v1.93zm-2.51 0h1.46V67h-1.48v1.93zm-2.86 0h1.46V67H92v1.93zM64.29 49h10v-2h-10v2zm-8.53 47.57h6.16V44.23h20.76v52.34h3V64.13h16V24.55h20.76v72h4.33V14.74l20.76 9.52v72.31h5.3v5H55.76v-5z"/>
                        </svg>
                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Warstwa 1" viewBox="0 0 208.63 208.63">
                            <defs>
                                <clipPath id="a">
                                    <path fill="none" d="M0 0h213.08v208.63H0z"/>
                                </clipPath>
                            </defs>
                            <path fill="#1abfd5" d="M0 0h208.63v208.63H0z"/>
                            <g clip-path="url(#a)">
                                <path fill="#fff" d="M114.54 47a10.24 10.24 0 1 1-10.24-10.23A10.24 10.24 0 0 1 114.54 47"/>
                            </g>
                            <path fill="#fff" d="M90.68 102.69h27.25l7.73 9.17H82.93l7.75-9.17"/>
                            <g clip-path="url(#a)">
                                <path fill="#fff" d="M104.31 26.8A20.21 20.21 0 1 0 124.52 47a20.21 20.21 0 0 0-20.21-20.2m22.43 31.26c-.25.52-.53 1-.83 1.54l-21.6 39.86-21.62-39.85c-.31-.52-.58-1-.85-1.54v-.08a25.06 25.06 0 1 1 47.55-11.06 24.77 24.77 0 0 1-2.6 11.07z"/>
                            </g>
                            <text fill="#fff" transform="translate(10.69 147.97)" font-size="21" font-family="Lato" font-weight="700">
                                Z<tspan x="12.36" y="0">L</tspan><tspan x="22.57" y="0">O</tspan><tspan x="39.44" y="0">K</tspan><tspan x="54.38" y="0">A</tspan><tspan x="68.84" y="0">L</tspan><tspan x="79.24" y="0">I</tspan><tspan x="84.84" y="0">Z</tspan><tspan x="96.78" y="0">O</tspan><tspan x="113.11" y="0" letter-spacing="-1">W</tspan><tspan x="134.03" y="0">A</tspan><tspan x="148.49" y="0">N</tspan><tspan x="164.61" y="0">E</tspan>
                            </text>
                            <text fill="#fff" transform="translate(10.69 170.96)" font-size="21" font-family="Lato" font-weight="700">
                                W <tspan x="27.17" y="0">C</tspan><tspan x="40.69" y="0">E</tspan><tspan x="52.51" y="0">N</tspan><tspan x="68.42" y="0">T</tspan><tspan x="80.72" y="0">R</tspan><tspan x="93.74" y="0">U</tspan><tspan x="109.31" y="0">M</tspan>
                            </text>
                            <text fill="#fff" transform="translate(142.04 170.96)" font-size="21" font-family="Lato" font-weight="700">

                            </text>
                            <text fill="#fff" transform="translate(10.69 193.96)" font-size="21" font-family="Lato" font-weight="700">
                                K<tspan x="14.14" y="0">R</tspan><tspan x="28.11" y="0">A</tspan><tspan x="43.11" y="0">J</tspan><tspan x="52.09" y="0">U</tspan>
                            </text>
                        </svg>
                        <svg class="svg-box _nd-placegolder" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
							</text>
							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
							</text>
							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
								2
							</text>
						</svg>


                        <svg class="svg-box _long" id="Warstwa_1" data-name="Warstwa 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 423.63 222.26"><defs><clipPath id="clip-path"><rect x="-9" y="-7" width="441.63" height="222.9" fill="none"/></clipPath></defs><title>Vector Smart Object3</title><rect width="423.63" height="208.63" fill="#1abfd5"/><g clip-path="url(#clip-path)"><path d="M212.36,110.8a4.93,4.93,0,0,0-6.45,2.63l-1.73,4.5h0a5.07,5.07,0,0,0,1.36,5.38c1.95,1.84,6.13,2.1,7.25-.31l2.55-5.89a4.93,4.93,0,0,0-3-6.3" fill="#fff"/><path d="M205,103.38a4.93,4.93,0,0,0-6.8,1.53l-3.15,5.25h0a5,5,0,0,0,.65,5.84c1.84,2.11,5.54,2.47,6.94.66s4.27-6.59,4.27-6.59a4.93,4.93,0,0,0-1.89-6.71" fill="#fff"/><path d="M198.43,95.46a4.93,4.93,0,0,0-6.91.9l-4.65,6.12h0a5.07,5.07,0,0,0,7.49,6.79l5.33-7a4.93,4.93,0,0,0-1.27-6.85" fill="#fff"/><path d="M191.13,89a4.93,4.93,0,0,0-7,0l-3.72,4a5.08,5.08,0,0,0,6.93,7.34h0c0.54-.55,4.16-4.37,4.16-4.37a4.93,4.93,0,0,0-.41-7" fill="#fff"/><path d="M247,47.59l-35.61-3.21-26.89,18h0l0,0h0l0,0.08h0l0,0.07h0l0,0.06v0l0,0.06v0l0,0.06,0,0,0,0.07,0,0,0,0.08,0,0,0.06,0.09v0l0.17,0.25v0l0.08,0.11,0,0,0.08,0.1,0,0,0.08,0.1,0,0,0.09,0.11,0,0,0.1,0.11,0,0,0.12,0.13h0c0.14,0.15.29,0.31,0.46,0.47l0,0,0.14,0.13,0,0,0.14,0.12,0,0,0.14,0.12,0,0L187,65.36l0,0,0.18,0.13,0,0q0.31,0.23.66,0.44l0,0,0.2,0.11,0.05,0,0.19,0.1,0.06,0,0.19,0.09,0.07,0,0.2,0.09,0.07,0,0.22,0.09,0.05,0a9,9,0,0,0,.88.29h0l0.18,0,0.3,0.07h0l0.19,0h0c0.27,0.05.54,0.09,0.82,0.13h0a10.72,10.72,0,0,0,1.34.05h0.42c0.36,0,.73-0.06,1.12-0.12h0l0.86-.16h0a16.32,16.32,0,0,0,1.69-.48l0.11,0h0l0.13,0h0l0.4-.15,0.12-.07,0,0,0,0,0.06,0,0,0h0l0.09-.06h0l0.15-.09h0l0,0,0.12-.07h0l0.18-.1h0l0.23-.13,0.08,0,0.16-.09,0.1-.05,0.19-.1,0.2-.1,0.1-.05L200,65l0,0c4.06-2.09,11.17-5.28,11.17-5.28l0,0,1.21,1a60.63,60.63,0,0,0,16.94,10.29l0.56,0.21L233,74.68,246.51,89l6.3-5.08,1.73-.22Z" fill="#fff"/><path d="M244.68,90.84L231.15,76.41l-2.73-2.95a63,63,0,0,1-17.7-10.74c-3.6,1.64-9.87,4.57-11.58,5.66l-0.22.14-0.24.09a16.82,16.82,0,0,1-6,1.19,11.71,11.71,0,0,1-10.53-6.39l-0.9-2L205,45.56l-28.47,2.31L169.1,83.65l1.34,0.2,10.62,5.7,1.71-1.82a6.52,6.52,0,0,1,4.81-2.06,7.15,7.15,0,0,1,7.09,6.68c0,0.08,0,.16,0,0.23,0.23,0,.45,0,0.68,0a7.17,7.17,0,0,1,7,5.81,6.69,6.69,0,0,1,0,2.38,7.14,7.14,0,0,1,3.63,1,7.06,7.06,0,0,1,3.21,4.15,6.65,6.65,0,0,1,.19,2.71A7.18,7.18,0,0,1,213,109a7.06,7.06,0,0,1,3.85,3.56,6.52,6.52,0,0,1,.2,5.19l0,0.06-1.91,4.41c1.46,0.21,3.27-.66,4.73-2a5.07,5.07,0,0,0,.31-6.62l-9-9.51,0.7-.76,9.38,9.94a5.05,5.05,0,0,0,7.07-.15c2-1.92,2.34-4.62.52-6.55L218.82,95.86l0.71-.78,10,10.55a5.24,5.24,0,0,0,7.22,0,4.76,4.76,0,0,0,.22-6.83L226.41,87.62l0.69-.75,10.27,10.86a4.9,4.9,0,0,0,6.94,0,4.81,4.81,0,0,0,.38-6.86" fill="#fff"/><path d="M152.65,80.14a2.66,2.66,0,1,1,2.65-2.65,2.65,2.65,0,0,1-2.65,2.65m17.71-39.53-20.51-4.29a3.77,3.77,0,0,0-.78-0.08l-9.64,46.1a3.75,3.75,0,0,0,.75.24l20.51,4.29a3.79,3.79,0,0,0,4.49-2.93l8.12-38.83a3.79,3.79,0,0,0-2.93-4.49" fill="#fff"/><path d="M270.67,80.72a2.65,2.65,0,1,1,2.65-2.65,2.65,2.65,0,0,1-2.65,2.65m3.89-44.48a3.77,3.77,0,0,0-.78.08l-20.51,4.29a3.79,3.79,0,0,0-2.93,4.49l8.12,38.83a3.79,3.79,0,0,0,4.49,2.93l20.51-4.29a3.8,3.8,0,0,0,.75-0.24Z" fill="#fff"/></g><text transform="translate(65.72 175.76)" font-size="21" fill="#fff" font-family="Lato" font-weight="700">M<tspan x="19.61" y="0">I</tspan><tspan x="25.59" y="0">E</tspan><tspan x="37.66" y="0">J</tspan><tspan x="46.49" y="0">S</tspan><tspan x="57.93" y="0">C</tspan><tspan x="71.44" y="0">E W </tspan><tspan x="115.79" y="0">R</tspan><tspan x="129.76" y="0">A</tspan><tspan x="144.22" y="0">N</tspan><tspan x="160.29" y="0">K</tspan><tspan x="174.33" y="0">I</tspan><tspan x="180.3" y="0">N</tspan><tspan x="196.44" y="0">G</tspan><tspan x="211.63" y="0">U </tspan><tspan x="232.32" y="0">M</tspan><tspan x="251.93" y="0">I</tspan><tspan x="257.82" y="0">A</tspan><tspan x="272.56" y="0">S</tspan><tspan x="284.12" y="0">T</tspan></text><text transform="translate(65.72 198.75)" font-size="21" fill="#fff" font-family="Lato" font-weight="700">P<tspan x="12.49" y="0">R</tspan><tspan x="26.17" y="0">Z</tspan><tspan x="39.26" y="0" letter-spacing="-1">Y</tspan><tspan x="51.12" y="0">J</tspan><tspan x="59.37" y="0">A</tspan><tspan x="74.24" y="0">Z</tspan><tspan x="86.62" y="0">N</tspan><tspan x="102.72" y="0" letter-spacing="-1">Y</tspan><tspan x="115.26" y="0">C</tspan><tspan x="128.77" y="0">H </tspan><tspan x="150.05" y="0">B</tspan><tspan x="163.65" y="0">I</tspan><tspan x="169.25" y="0">Z</tspan><tspan x="181.63" y="0">N</tspan><tspan x="197.74" y="0">E</tspan><tspan x="209.82" y="0">S</tspan><tspan x="221.21" y="0">O</tspan><tspan x="237.55" y="0">W</tspan><tspan x="259.26" y="0">I</tspan></text><text transform="translate(11.66 199.46)" font-size="80" fill="#fff" font-family="Lato" font-weight="700">5</text></svg>

                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Warstwa 1" viewBox="0 0 208.63 208.74">
                            <defs>
                                <clipPath id="a">
                                    <path fill="none" d="M0 0h212.92v208.74H0z"/>
                                </clipPath>
                            </defs>
                            <path fill="#00b188" d="M0 0h208.63v208.74H0z"/>
                            <g fill="#fff" clip-path="url(#a)">
                                <path d="M117.76 122.18L95.66 125l-1.19 1.35a1 1 0 0 0 .39 1.55c1.69.65 4.23 1.93 4.23 1.93l3.51 2.41a7.88 7.88 0 0 0 8.43-.93l3.41-3.42a21.3 21.3 0 0 1 3.87-3 1.13 1.13 0 0 0 .36-1.8zm1-13.32L94 111.43l-1 .9a1.11 1.11 0 0 0 .12 1.77 4.21 4.21 0 0 1 1.79 2.65l22.94-2.67a3.65 3.65 0 0 1 1.41-2.35 1.42 1.42 0 0 0 .2-2.06zm-.46 6.7l-23.49 2.68-1 .89a1.12 1.12 0 0 0 .12 1.73 4.13 4.13 0 0 1 1.72 2.59l21.79-2.77a3.6 3.6 0 0 1 1.32-2.31 1.43 1.43 0 0 0 .18-2zm9.47-39.69a21.17 21.17 0 0 1-4.87 13.5 35.31 35.31 0 0 0-6.33 13.57L96.73 105A38.36 38.36 0 0 0 90 88.93a21.19 21.19 0 0 1 16.13-34.2h.51a21.16 21.16 0 0 1 21.16 21.15m4.6 0a25.78 25.78 0 0 0-25.76-25.75H106a25.75 25.75 0 0 0-19.65 41.64 33.65 33.65 0 0 1 6.11 16.11 2.13 2.13 0 0 0 2.11 2h.22l23.93-2.49a2.28 2.28 0 0 0 2-1.83c.92-4.9 3.22-10.2 5.71-13.2a25.78 25.78 0 0 0 5.93-16.44m-54.18.01a2.3 2.3 0 0 0-2.3-2.3h-16.8a2.3 2.3 0 1 0 0 4.6h16.8a2.3 2.3 0 0 0 2.3-2.3m75.96-2.3h-16.8a2.3 2.3 0 0 0 0 4.6h16.8a2.3 2.3 0 0 0 0-4.6"/>
                                <path d="M84.88 95.36a2.28 2.28 0 0 0-1.63.67l-11.87 11.88a2.3 2.3 0 1 0 3.25 3.25l11.88-11.87a2.3 2.3 0 0 0-1.63-3.93m43.46-38.86a2.28 2.28 0 0 0 1.63-.67L141.84 44a2.3 2.3 0 1 0-3.25-3.25l-11.88 11.83a2.3 2.3 0 0 0 1.63 3.93m-21.73-9.01a2.3 2.3 0 0 0 2.3-2.3V28.41a2.3 2.3 0 1 0-4.6 0V45.2a2.3 2.3 0 0 0 2.3 2.3M130 96a2.3 2.3 0 1 0-3.25 3.25l11.87 11.88a2.3 2.3 0 1 0 3.25-3.25zM83.25 55.83a2.3 2.3 0 0 0 3.25-3.25L74.63 40.7a2.3 2.3 0 0 0-3.25 3.3z"/>
                            </g>
                            <text fill="#fff" transform="translate(9.3 171.59)" font-size="21" font-family="Lato" font-weight="700">
                                M<tspan x="19.59" y="0">I</tspan><tspan x="25.59" y="0">E</tspan><tspan x="37.66" y="0">J</tspan><tspan x="46.49" y="0">S</tspan><tspan x="57.93" y="0">C</tspan><tspan x="71.44" y="0">E </tspan><tspan x="88.62" y="0">D</tspan><tspan x="104.53" y="0">L</tspan><tspan x="115.81" y="0">A</tspan>
                            </text>
                            <text fill="#fff" transform="translate(142.69 171.59)" font-size="21" font-family="Lato" font-weight="700">

                            </text>
                            <text fill="#fff" transform="translate(9.3 194.58)" font-size="21" font-family="Lato" font-weight="700">
                                I<tspan x="6" y="0">NN</tspan><tspan x="38.23" y="0">O</tspan><tspan x="54.57" y="0" letter-spacing="-1">W</tspan><tspan x="75.48" y="0">A</tspan><tspan x="89.33" y="0">C</tspan><tspan x="103.27" y="0">J</tspan><tspan x="112.03" y="0">I</tspan>
                            </text>
                        </svg>

                        <svg class="svg-box" id="Warstwa_1" data-name="Warstwa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 208.63 208.63"><title>Vector Smart Object5</title><rect width="208.63" height="208.63" fill="#79bc43"/><text transform="translate(8.94 173.51)" font-size="21" fill="#fff" font-family="Lato" font-weight="700">B<tspan x="13.62" y="0">O</tspan><tspan x="30.57" y="0">G</tspan><tspan x="45.69" y="0" letter-spacing="-1">A</tspan><tspan x="58.46" y="0">T</tspan><tspan x="70.81" y="0">E </tspan><tspan x="87.99" y="0">T</tspan><tspan x="100.29" y="0">R</tspan><tspan x="114.26" y="0">A</tspan><tspan x="128.72" y="0" letter-spacing="-1">D</tspan><tspan x="143.58" y="0" letter-spacing="-1">Y</tspan><tspan x="156.11" y="0">C</tspan><tspan x="170.05" y="0">J</tspan><tspan x="178.98" y="0">E</tspan></text><text transform="translate(8.94 196.5)" font-size="21" fill="#fff" font-family="Lato" font-weight="700">P<tspan x="12.49" y="0">R</tspan><tspan x="26.17" y="0">Z</tspan><tspan x="38.52" y="0">E</tspan><tspan x="50.35" y="0">M</tspan><tspan x="70.13" y="0">Y</tspan><tspan x="83.13" y="0">S</tspan><tspan x="94.79" y="0">Ł</tspan><tspan x="105.35" y="0">O</tspan><tspan x="121.68" y="0">W</tspan><tspan x="143.57" y="0">E</tspan></text><path d="M70.93,44.21H64.09v5.32h6.84V44.21Zm11.6,0h-8v5.32h8V44.21Zm10.47,0H86.14v5.32H93V44.21Zm11.33,0H96.61v5.32h7.72V44.21Zm15.32,0H108v5.32h11.7V44.21Zm7.61,5.32-0.86-5.32h-2.84v5.32h3.7Zm10.1,63.31-9.51-59.58h-4.29v59.58h13.8Zm1.83-68.62h-8.84l0.85,5.32h8v3.73h-7.41l9.53,59.58H147V124h3.35v4.47h-67V124H109.6V112.83h10V53.26h-62V49.53h2.8V44.21h-2.8V40.48h62V35.56h3.91v4.92h2.24l-0.65-4.09,3.86-.6,0.76,4.69h9.43v3.73Z" transform="translate(-0.06)" fill="#fff"/><polygon points="68.98 88.22 67.65 88.22 67.65 59.01 65.36 59.01 63.31 55.98 73.37 55.98 71.3 59.01 68.98 59.01 68.98 88.22" fill="#fff"/></svg>




                    </div>

                    <?php if( get_field('s1-title') ): ?>
                        <div class="section-copy">
                            <h1 class="heading"><?php the_field('s1-title') ?></h1>
                            <p class="copy"><?php the_field('s1-content') ?></p>
                        </div>
                    <?php endif; ?>

                </div>
				<div class="section-content googlemap-section">
                    <button id="map-handler" class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 554.625 554.625">
                            <g fill="#1fbed8">
                                <path d="M154.142,86.062c0-36.337,30.6-66.938,66.938-66.938s66.937,30.6,66.937,66.938v53.55    c11.476-15.3,19.125-34.425,19.125-53.55C307.141,38.25,268.891,0,221.079,0c-47.812,0-86.062,38.25-86.062,86.062    c0,21.038,7.65,40.163,19.125,53.55V86.062z"/>
                                <path d="M450.579,229.5c-11.475,0-21.037,3.825-28.688,9.562l0,0c0-26.775-21.037-47.812-47.812-47.812    c-11.475,0-22.949,3.825-32.512,11.475c-7.65-19.125-24.863-30.6-43.988-30.6c-11.475,0-21.037,3.825-28.688,9.562V86.062    c0-26.775-21.037-47.812-47.812-47.812s-47.812,21.038-47.812,47.812v170.212c-38.25-34.425-80.325-61.2-107.1-34.425    c-38.25,38.25,42.075,112.837,103.275,225.675c43.988,78.412,105.188,107.1,166.387,107.1    c89.888,0,162.562-72.675,162.562-162.562v-114.75C498.391,250.538,477.354,229.5,450.579,229.5z M479.266,319.388v72.675    c0,74.588-65.024,143.438-143.438,143.438c-72.675,0-114.75-40.162-149.175-95.625c-72.675-126.225-126.225-183.6-107.1-204.638    c21.037-21.037,72.675,24.863,112.837,66.938V86.062c0-15.3,13.387-28.688,28.688-28.688c15.3,0,28.688,13.388,28.688,28.688    v200.812h19.125v-66.938c0-15.3,13.388-28.688,28.688-28.688c15.301,0,28.688,13.388,28.688,28.688v47.812h19.125v-28.688    c0-15.3,13.388-28.688,28.688-28.688c15.301,0,28.688,13.388,28.688,28.688v47.812h19.125v-9.562    c0-15.3,13.388-28.688,28.688-28.688c15.301,0,28.688,13.388,28.688,28.688V319.388z"/>
                            </g>
                        </svg>                    </button>
                    <div class="map-overlay">
                    </div>
                    <div id="location-map" class="swiper-no-swiping">



                    </div>




                </div>
				<div class="section-content infrastructure-section">

                    <div class="bg" style="background-image: url('<?= the_field('bg2');?>')"></div>
                    <div class="infoboxes-container">


<!--                        <img src="--><?//= get_template_directory_uri();?><!--/dist/images/icons/location-2-1.svg" data-aos="flip-left"   alt="" class="svg-box">-->
<!--                        <img src="--><?//= get_template_directory_uri();?><!--/dist/images/icons/location-2-2.svg" data-aos="flip-left"   alt="" class="svg-box">-->
<!--                        <img src="--><?//= get_template_directory_uri();?><!--/dist/images/icons/location-2-3.svg" data-aos="flip-left"   alt="" class="svg-box">-->
<!--                        <img src="--><?//= get_template_directory_uri();?><!--/dist/images/icons/location-2-4.svg" data-aos="flip-left"   alt="" class="svg-box">-->
<!--                        <img src="" alt="" class="svg-box">-->
<!--                        <img src="" alt="" class="svg-box">-->
<!--                        <img src="--><?//= get_template_directory_uri();?><!--/dist/images/icons/location-2-5.svg" alt="" class="svg-box">-->


                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Warstwa 1" viewBox="0 0 208.63 423.63">
                            <path fill="#79bc43" d="M0 0h208.63v423.63H0z"/>
                            <path fill="#fff" d="M107.01 204.67v-91.73l13.64 13.64 3.82-3.82-20.16-20.15-20.15 20.15 3.82 3.82 13.63-13.64v91.73l-13.63-13.64-3.82 3.82L104.31 215l20.16-20.15-3.82-3.82-13.64 13.64"/>
                            <path fill="#fff" d="M58.45 161.5h58.46v.01h33.26l-13.63 13.63 3.82 3.82 20.15-20.15-20.15-20.16-3.82 3.82 13.63 13.64h-31.46v-.01H58.45l13.63-13.63-3.81-3.82-20.15 20.15 20.15 20.16 3.81-3.82-13.63-13.64"/>
                            <text fill="#fff" transform="translate(10.69 336.23)" font-size="19" font-family="Lato" font-weight="700">
                                N<tspan x="14.51" y="0">A </tspan><tspan x="32.32" y="0">S</tspan><tspan x="42.72" y="0">K</tspan><tspan x="55.54" y="0">R</tspan><tspan x="67.9" y="0">Z</tspan><tspan x="79.77" y="0">Y</tspan><tspan x="92.18" y="0">Ż</tspan><tspan x="103" y="0">O</tspan><tspan x="117.8" y="0" letter-spacing="-1">W</tspan><tspan x="136.72" y="0">A</tspan><tspan x="149.8" y="0">N</tspan><tspan x="164.23" y="0">I</tspan><tspan x="169.72" y="0">U</tspan>
                            </text>
                            <text fill="#fff" transform="translate(10.69 358.23)" font-size="19" font-family="Lato" font-weight="700">
                                K<tspan x="12.82" y="0">R</tspan><tspan x="25.45" y="0">A</tspan><tspan x="39.03" y="0">J</tspan><tspan x="47.05" y="0">O</tspan><tspan x="61.85" y="0">W</tspan><tspan x="82.39" y="0">Y</tspan><tspan x="93.74" y="0">C</tspan><tspan x="105.96" y="0">H </tspan><tspan x="125.22" y="0">D</tspan><tspan x="139.59" y="0">R</tspan><tspan x="151.33" y="0">Ó</tspan><tspan x="166.67" y="0">G</tspan>
                            </text>
                            <text fill="#fff" transform="translate(10.69 380.23)" font-size="19" font-family="Lato" font-weight="700">
                                W<tspan x="19.63" y="0">S</tspan><tspan x="29.99" y="0">C</tspan><tspan x="42.22" y="0">HÓ</tspan><tspan x="72.1" y="0">D</tspan><tspan x="86.81" y="0" letter-spacing="-1">-</tspan><tspan x="92.76" y="0">Z</tspan><tspan x="104.47" y="0">A</tspan><tspan x="117.02" y="0">C</tspan><tspan x="129.25" y="0">HÓ</tspan><tspan x="159.12" y="0">D</tspan>
                            </text>
                            <text fill="#fff" transform="translate(10.69 403.24)" font-size="19" font-family="Lato" font-weight="700">
                                P<tspan x="11.67" y="0">Ó</tspan><tspan x="27.11" y="0">Ł</tspan><tspan x="37" y="0">N</tspan><tspan x="51.58" y="0">O</tspan><tspan x="66.92" y="0" letter-spacing="-1">C</tspan><tspan x="78.4" y="0">-</tspan><tspan x="85.47" y="0">P</tspan><tspan x="97.14" y="0">O</tspan><tspan x="112.58" y="0">Ł</tspan><tspan x="122.17" y="0">U</tspan><tspan x="136.26" y="0">D</tspan><tspan x="150.67" y="0">NI</tspan><tspan x="170.52" y="0">E</tspan>
                            </text>
                            <text fill="#fff" transform="translate(191.49 403.23)" font-size="21" font-family="Lato" font-weight="700">

                            </text>
                        </svg>

                        <svg class="svg-box" id="Warstwa_1" data-name="Warstwa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 208.63 208.63"><title>Vector Smart Object1</title><rect width="208.63" height="208.63" fill="#1abfd5"/><polygon points="119.14 34.11 107.28 34.11 107.28 58.95 125.57 58.95 119.14 34.11" fill="#fff"/><polygon points="128.96 72.05 107.28 72.05 107.28 111.82 139.25 111.82 128.96 72.05" fill="#fff"/><polygon points="101.35 34.11 89.49 34.11 83.06 58.95 101.35 58.95 101.35 34.11" fill="#fff"/><polygon points="69.37 111.82 101.35 111.82 101.35 72.05 79.67 72.05 69.37 111.82" fill="#fff"/><polygon points="79.56 68.19 129.07 68.19 129.07 71.94 138.2 71.94 138.2 60.62 70.43 60.62 70.43 71.94 79.56 71.94 79.56 68.19" fill="#fff"/><text transform="translate(10.69 147.81)" font-size="18" fill="#fff" font-family="Lato" font-weight="700">A<tspan x="11.89" y="0">U</tspan><tspan x="25.15" y="0">T</tspan><tspan x="35.41" y="0">O</tspan><tspan x="49.79" y="0">S</tspan><tspan x="59.71" y="0">T</tspan><tspan x="70.28" y="0">R</tspan><tspan x="82.27" y="0">A</tspan><tspan x="94.66" y="0">D</tspan><tspan x="107.41" y="0">Y </tspan><tspan x="123.45" y="0">A</tspan><tspan x="135.3" y="0">1 I </tspan><tspan x="159.83" y="0">A</tspan><tspan x="172.49" y="0">2</tspan><tspan x="183.24" y="0">,</tspan></text><text transform="translate(195.95 147.81)" font-size="18" fill="#fff" font-family="Lato" font-weight="700"> </text><text transform="translate(10.69 169.8)" font-size="18" fill="#fff" font-family="Lato" font-weight="700">D<tspan x="13.62" y="0">R</tspan><tspan x="24.74" y="0">O</tspan><tspan x="39.28" y="0">G</tspan><tspan x="52.26" y="0">A </tspan><tspan x="69.14" y="0">E</tspan><tspan x="79.25" y="0">K</tspan><tspan x="91.48" y="0">S</tspan><tspan x="101.34" y="0">P</tspan><tspan x="112.07" y="0">R</tspan><tspan x="123.42" y="0">E</tspan><tspan x="133.77" y="0">S</tspan><tspan x="143.56" y="0">O</tspan><tspan x="157.58" y="0">W</tspan><tspan x="175.52" y="0">A</tspan></text><text transform="translate(10.69 191.8)" font-size="18" fill="#fff" font-family="Lato" font-weight="700">S8 I <tspan x="34.4" y="0" letter-spacing="-1">S</tspan><tspan x="43.15" y="0">1</tspan><tspan x="52.84" y="0">4</tspan></text></svg>

                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Warstwa 1" viewBox="0 0 208.63 208.63">
                            <defs>
                                <clipPath id="a">
                                    <path fill="none" d="M-9-7h226.63v216.63H-9z"/>
                                </clipPath>
                            </defs>
                            <path fill="#00b188" d="M0 0h208.63v208.63H0z"/>
                            <g clip-path="url(#a)">
                                <path fill="#fff" d="M120.6 78.83l3.16 11.61s2.15 6.77-1.19 9l-3 2L113 84l-5.51 3.71a.88.88 0 0 1-1.11-.52 1 1 0 0 1 0-1.27l5.83-3.92-6.15-16.23L81.2 82.52l2.25 11.1a11 11 0 0 1-.75 2.18l-9.23-14.63-3.7 2.52a1.76 1.76 0 0 1-2.46-.53 1.88 1.88 0 0 1 .52-2.54l3.7-2.52-9.21-14.6a9.28 9.28 0 0 1 2.23.16l8.9 6.58 24.84-16.82-12.44-13-5.71 3.88a.91.91 0 0 1-1.14-.48 1 1 0 0 1 0-1.29l5.37-3.61L72.14 26.2l3-2c3.33-2.25 8.44 2.48 8.44 2.48l8.35 7.12 4.14-2.8a.91.91 0 0 1 1.12.54 1 1 0 0 1 0 1.26l-3.59 2.44 14 11.91 18.52-12.55c12.22-8.27 18.34-3.6 18.34-3.6s1.65 7.65-10.57 15.92l-18.56 12.53L120 76.66l3.41-2.29a.89.89 0 0 1 1.1.51 1 1 0 0 1 0 1.28z"/>
                            </g>
                            <text fill="#fff" transform="translate(8.02 132.75)" font-size="18" font-family="Lato" font-weight="700">
                                1<tspan x="10.08" y="0">5</tspan><tspan x="20.36" y="0"> </tspan><tspan x="24.55" y="0">M</tspan><tspan x="41.18" y="0">I</tspan><tspan x="46.16" y="0">N</tspan><tspan x="59.87" y="0"> </tspan><tspan x="64.06" y="0">D</tspan><tspan x="77.55" y="0">O </tspan><tspan x="96.11" y="0">L</tspan><tspan x="104.7" y="0">O</tspan><tspan x="118.53" y="0">TN</tspan><tspan x="142.44" y="0">I</tspan><tspan x="147.42" y="0">S</tspan><tspan x="157.09" y="0">K</tspan><tspan x="169.72" y="0">A</tspan>
                            </text>
                            <text fill="#fff" transform="translate(186.9 132.75)" font-size="18" font-family="Lato" font-weight="700">

                            </text>
                            <text fill="#fff" transform="translate(8.02 154.74)" font-size="18" font-family="Lato" font-weight="700">
                                W <tspan x="22.57" y="0">Ł</tspan><tspan x="31.27" y="0">O</tspan><tspan x="45.42" y="0" letter-spacing="-1">D</tspan><tspan x="57.94" y="0">Z</tspan><tspan x="68.08" y="0">I I </tspan><tspan x="86.07" y="0">1</tspan><tspan x="96.29" y="0">,</tspan><tspan x="100.41" y="0">5</tspan><tspan x="110.51" y="0" white-space="pre"> H </tspan><tspan x="132.05" y="0">D</tspan><tspan x="145.36" y="0">O</tspan>
                            </text>
                            <text fill="#fff" transform="translate(8.02 176.74)" font-size="18" font-family="Lato" font-weight="700">
                                L<tspan x="8.41" y="0">O</tspan><tspan x="22.06" y="0">TN</tspan><tspan x="45.61" y="0">I</tspan><tspan x="50.41" y="0">S</tspan><tspan x="59.9" y="0">K</tspan><tspan x="72.35" y="0">A</tspan><tspan x="84.51" y="0"> </tspan><tspan x="88.72" y="0">C</tspan><tspan x="100.14" y="0">H</tspan><tspan x="113.79" y="0">O</tspan><tspan x="128.09" y="0">P</tspan><tspan x="138.56" y="0">I</tspan><tspan x="143.54" y="0">N</tspan><tspan x="157.12" y="0">A</tspan>
                            </text>
                            <text fill="#fff" transform="translate(8.02 198.74)" font-size="18" font-family="Lato" font-weight="700">
                                W <tspan x="22.97" y="0" letter-spacing="-1">W</tspan><tspan x="40.73" y="0">A</tspan><tspan x="52.93" y="0">R</tspan><tspan x="64.18" y="0">S</tspan><tspan x="73.57" y="0">Z</tspan><tspan x="84.48" y="0" letter-spacing="-1">A</tspan><tspan x="95.85" y="0">W</tspan><tspan x="114.3" y="0">I</tspan><tspan x="119.26" y="0">E</tspan>
                            </text>
                        </svg>

                        <svg class="svg-box _hr-rectangle" id="Warstwa_1" data-name="Warstwa 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 423.63 208.63"><defs><clipPath id="clip-path"><rect x="-9" y="-7" width="441.63" height="216.63" fill="none"/></clipPath></defs><title>Vector Smart Object3</title><rect width="423.63" height="208.63" fill="#1abfd5"/><g clip-path="url(#clip-path)"><path d="M204.32,123H150.58V87.29h53.74V123ZM175.11,92.18a1.19,1.19,0,0,0-.88-0.41A1.32,1.32,0,0,0,173,93.19v24a1.51,1.51,0,0,0,.36,1,1.16,1.16,0,0,0,.87.42,1.34,1.34,0,0,0,1.24-1.41v-24a1.51,1.51,0,0,0-.36-1m6.17-.41A1.33,1.33,0,0,0,180,93.19v24a1.52,1.52,0,0,0,.37,1,1.19,1.19,0,0,0,.88.42,1.33,1.33,0,0,0,1.23-1.41v-24a1.54,1.54,0,0,0-.35-1,1.18,1.18,0,0,0-.88-0.41m-21.47.41a1.2,1.2,0,0,0-.88-0.41,1.34,1.34,0,0,0-1.24,1.41v24a1.51,1.51,0,0,0,.37,1,1.19,1.19,0,0,0,.88.42,1.35,1.35,0,0,0,1.24-1.41v-24a1.51,1.51,0,0,0-.37-1m29-.41a1.32,1.32,0,0,0-1.24,1.41v24a1.54,1.54,0,0,0,.35,1,1.17,1.17,0,0,0,.88.42,1.33,1.33,0,0,0,1.23-1.41v-24a1.51,1.51,0,0,0-.36-1,1.17,1.17,0,0,0-.87-0.41m7.05,0a1.32,1.32,0,0,0-1.24,1.41v24a1.53,1.53,0,0,0,.35,1,1.19,1.19,0,0,0,.89.42,1.34,1.34,0,0,0,1.24-1.41v-24a1.51,1.51,0,0,0-.37-1,1.17,1.17,0,0,0-.88-0.41m-29.9,0a1.32,1.32,0,0,0-1.24,1.41v24a1.51,1.51,0,0,0,.36,1,1.16,1.16,0,0,0,.88.42,1.34,1.34,0,0,0,1.23-1.41v-24a1.53,1.53,0,0,0-.36-1,1.17,1.17,0,0,0-.87-0.41" fill="#fff"/><path d="M143.59,123H89.85V87.29h53.73V123ZM99.09,92.18a1.17,1.17,0,0,0-.88-0.41A1.32,1.32,0,0,0,97,93.19v24a1.53,1.53,0,0,0,.36,1,1.16,1.16,0,0,0,.87.42,1.34,1.34,0,0,0,1.24-1.41v-24a1.51,1.51,0,0,0-.37-1m6.17-.41A1.34,1.34,0,0,0,104,93.19v24a1.51,1.51,0,0,0,.37,1,1.19,1.19,0,0,0,.88.42,1.34,1.34,0,0,0,1.24-1.41v-24a1.53,1.53,0,0,0-.36-1,1.19,1.19,0,0,0-.88-0.41m16.17,0.41a1.17,1.17,0,0,0-.88-0.41,1.31,1.31,0,0,0-1.23,1.41v24a1.53,1.53,0,0,0,.35,1,1.15,1.15,0,0,0,.87.42,1.34,1.34,0,0,0,1.24-1.41v-24a1.51,1.51,0,0,0-.37-1m-7.92-.41a1.32,1.32,0,0,0-1.23,1.41v24a1.5,1.5,0,0,0,.36,1,1.15,1.15,0,0,0,.87.42,1.35,1.35,0,0,0,1.24-1.41v-24a1.5,1.5,0,0,0-.37-1,1.19,1.19,0,0,0-.88-0.41M129,92.18a1.18,1.18,0,0,0-.88-0.41,1.33,1.33,0,0,0-1.24,1.41v24a1.51,1.51,0,0,0,.37,1,1.17,1.17,0,0,0,.88.42,1.34,1.34,0,0,0,1.24-1.41v-24a1.51,1.51,0,0,0-.36-1m7.05,0a1.15,1.15,0,0,0-.87-0.41,1.33,1.33,0,0,0-1.25,1.41v24a1.52,1.52,0,0,0,.37,1,1.19,1.19,0,0,0,.88.42,1.34,1.34,0,0,0,1.23-1.41v-24a1.53,1.53,0,0,0-.36-1" fill="#fff"/><path d="M160.47,75.2v-24a1.47,1.47,0,0,0-.37-1,1.17,1.17,0,0,0-.88-0.41A1.33,1.33,0,0,0,158,51.22v24a1.46,1.46,0,0,0,.37,1,1.16,1.16,0,0,0,.87.41,1.32,1.32,0,0,0,1.24-1.4M129.33,49.83a1.31,1.31,0,0,0-1.24,1.39v24a1.47,1.47,0,0,0,.36,1,1.15,1.15,0,0,0,.88.41,1.32,1.32,0,0,0,1.24-1.4v-24a1.5,1.5,0,0,0-.36-1,1.19,1.19,0,0,0-.88-0.41m37,0A1.32,1.32,0,0,0,165,51.22v24a1.46,1.46,0,0,0,.37,1,1.16,1.16,0,0,0,.88.41,1.33,1.33,0,0,0,1.24-1.4v-24a1.47,1.47,0,0,0-.37-1,1.19,1.19,0,0,0-.88-0.41m-14.62,0a1.31,1.31,0,0,0-1.24,1.39v24a1.49,1.49,0,0,0,.35,1,1.17,1.17,0,0,0,.88.41,1.33,1.33,0,0,0,1.24-1.4v-24a1.47,1.47,0,0,0-.37-1,1.18,1.18,0,0,0-.88-0.41m-15.29,0a1.32,1.32,0,0,0-1.24,1.39v24a1.49,1.49,0,0,0,.36,1,1.16,1.16,0,0,0,.88.41,1.31,1.31,0,0,0,1.22-1.4v-24a1.5,1.5,0,0,0-.35-1,1.15,1.15,0,0,0-.87-0.41m8.24,0a1.32,1.32,0,0,0-1.24,1.39v24a1.46,1.46,0,0,0,.37,1,1.16,1.16,0,0,0,.88.41,1.32,1.32,0,0,0,1.24-1.4v-24a1.48,1.48,0,0,0-.37-1,1.16,1.16,0,0,0-.87-0.41M121,45.35h53.75v35.7H121V45.35Z" fill="#fff"/></g><text transform="translate(11.02 167.4)" font-size="19" fill="#fff" font-family="Lato" font-weight="700">B<tspan x="12.44" y="0">L</tspan><tspan x="21.85" y="0">I</tspan><tspan x="27.29" y="0">S</tspan><tspan x="37.7" y="0">K</tspan><tspan x="49.83" y="0">O </tspan><tspan x="69.8" y="0">K</tspan><tspan x="81.93" y="0">O</tspan><tspan x="97.22" y="0">L</tspan><tspan x="106.79" y="0">E</tspan><tspan x="117.71" y="0">J</tspan><tspan x="125.76" y="0">O</tspan><tspan x="140.56" y="0">W</tspan><tspan x="160.38" y="0">E</tspan><tspan x="170.77" y="0">G</tspan><tspan x="184.45" y="0">O </tspan><tspan x="204.42" y="0">T</tspan><tspan x="215.61" y="0">E</tspan><tspan x="226.27" y="0">R</tspan><tspan x="238.26" y="0">MI</tspan><tspan x="261.43" y="0">N</tspan><tspan x="275.93" y="0">A</tspan><tspan x="289.02" y="0">L</tspan><tspan x="298.3" y="0">U </tspan></text><text transform="translate(11.02 189.4)" font-size="19" fill="#fff" font-family="Lato" font-weight="700">K<tspan x="12.13" y="0">O</tspan><tspan x="27.43" y="0">N</tspan><tspan x="41.84" y="0">T</tspan><tspan x="53.03" y="0">E</tspan><tspan x="63.73" y="0">N</tspan><tspan x="78.31" y="0">E</tspan><tspan x="88.97" y="0">R</tspan><tspan x="100.71" y="0">O</tspan><tspan x="115.51" y="0">W</tspan><tspan x="135.33" y="0">E</tspan><tspan x="145.72" y="0">G</tspan><tspan x="159.4" y="0">O</tspan></text><g clip-path="url(#clip-path)"><path d="M216.44,78.69c-0.08-.6.38-8.85,0.49-10.5a0.15,0.15,0,0,0-.23-0.14L213,70.16a0.47,0.47,0,0,0-.19.2,9.58,9.58,0,0,0-1,4.14,13.31,13.31,0,0,0,1,5.2,0.51,0.51,0,0,0,.3.28L216,81a0.51,0.51,0,0,0,.67-0.59c-0.16-.78-0.23-1.43-0.27-1.73" fill="#fff"/><path d="M255.87,45.51c-7.15,4.09-15,8.6-22,12.62a0.51,0.51,0,0,0-.25.35c-0.19,1.08-.29,2-0.44,2.82-0.37,1.85-.37,15.06-0.22,19.19a47.27,47.27,0,0,0,.81,6.43,0.51,0.51,0,0,0,.34.39l21.23,7.42a0.51,0.51,0,0,0,.64-0.68,45.46,45.46,0,0,1-2.55-11.14c-0.24-1.94.73-22.58,1.33-30.47a16.6,16.6,0,0,1,1.79-6.26,0.51,0.51,0,0,0-.71-0.67" fill="#fff"/><path d="M228.19,80.43C228,79.25,228.63,66.7,229,61.9h0a0.51,0.51,0,0,0-.76-0.49l-8.54,4.92a0.51,0.51,0,0,0-.25.36c-0.08.5-.13,1-0.2,1.32-0.21,1-.21,8.42-0.12,10.73a24.77,24.77,0,0,0,.38,3.21,0.51,0.51,0,0,0,.34.39l8.42,2.94a0.51,0.51,0,0,0,.66-0.61,34.2,34.2,0,0,1-.74-4.23" fill="#fff"/><path d="M340.26,88.14c-3,.41-5.77-2.15-6.27-5.73s1.5-6.81,4.47-7.22,5.77,2.16,6.27,5.73-1.5,6.81-4.47,7.22m-35.4-22.76a19.07,19.07,0,0,1-7.1-5.16c-5.76-8.58-6.44-9.58-9.83-16.94-1.17-2.53.25-7.28,4.19-8.74,8.68-3.21,25-2.72,32.35,7.59,7.58,10.6,16.42,22.09,9.12,23.76-3.2.73-19.8,2.52-28.73-.51m13.28,26.32c-3.3,1-7-1.34-8.27-5.33s0.39-8.07,3.7-9.12,7,1.34,8.27,5.33-0.39,8.07-3.7,9.12m29.69-6.52C342.2,53.63,328,39.56,322.26,34.88c-12.35-10-35.9-5.89-43.43-2.35-0.92.43-6.14,3.38-13.29,7.45a3.91,3.91,0,0,0-1.67,1.85c-1.58,3.71-1.68,7.24-2.15,9.64-0.61,3-.61,24.77-0.37,31.56A84.64,84.64,0,0,0,263,95.33a4,4,0,0,0,2.6,3l16,5.6s5.9,1.89,12.53,3.83c5.92,1.74,12.44,3.54,15.87,4.09a73.12,73.12,0,0,0,16.3.56,75.89,75.89,0,0,0,8.2-1,17.55,17.55,0,0,0,6.76-2.48c2.25-1.5,3.91-3.69,5.51-6.87l0.19-.38c1.38-2.83,1.89-10.53.82-16.48" fill="#fff"/><path d="M347.7,121.84l-0.39-4.9a1.63,1.63,0,0,0-.9-1.43l-3.73-1.39a3.55,3.55,0,0,0-2.55,0,62.15,62.15,0,0,1-12,3.16c-14.15,2.51-39.45-6.22-39.45-6.22L219.58,85.36c-0.81-.3-1.54.59-1.4,1.71l0.1,0.79a1.62,1.62,0,0,0,.86,1.33l100.51,41.39c0.79,0.32,1.53-.5,1.45-1.61l-0.39-4.9c0-.07,0-0.14,0-0.21a1,1,0,0,1,.8-1.24l14.38-2.89a3.14,3.14,0,0,1,1.82.18l8.59,3.54c0.79,0.33,1.53-.5,1.45-1.61" fill="#fff"/></g></svg>
                        <svg class="svg-box _nd-placegolder" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
                                                        <path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
                                                        <text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
                                                            O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
                                                        </text>
                                                        <path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
                                                        <text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
                                                            1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
                                                        </text>
                                                        <text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
                                                            2
                                                        </text>
                                                        <text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
                                                            <tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
                                                        </text>
                                                        <text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
                                                            2
                                                        </text>
                                                    </svg>
                        <svg class="svg-box _nd-placegolder" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
                                                        <path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>
                                                        <text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">
                                                            O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
                                                        </text>
                                                        <path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>
                                                        <text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
                                                            1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>
                                                        </text>
                                                        <text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
                                                            2
                                                        </text>
                                                        <text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">
                                                            <tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>
                                                        </text>
                                                        <text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">
                                                            2
                                                        </text>
                                                    </svg>


                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Warstwa 1" viewBox="0 0 208.63 208.63">
                            <defs>
                                <clipPath id="a">
                                    <path fill="none" d="M0 0h208.63v208.63H0z"/>
                                </clipPath>
                            </defs>
                            <path fill="#79bc43" d="M0 0h208.63v208.63H0z"/>
                            <text fill="#fff" transform="translate(10.69 165.6)" font-size="19" font-family="Lato" font-weight="700">
                                T<tspan x="11.19" y="0">E</tspan><tspan x="21.85" y="0">R</tspan><tspan x="33.84" y="0">M</tspan><tspan x="51.58" y="0">I</tspan><tspan x="56.99" y="0">N</tspan><tspan x="71.5" y="0">A</tspan><tspan x="84.58" y="0">L </tspan><tspan x="99.06" y="0">C</tspan><tspan x="111.6" y="0">A</tspan><tspan x="124.64" y="0">R</tspan><tspan x="136.4" y="0">G</tspan><tspan x="150.08" y="0">O</tspan>
                            </text>
                            <text fill="#fff" transform="translate(10.69 187.6)" font-size="19" font-family="Lato" font-weight="700">
                                N<tspan x="14.51" y="0">A </tspan><tspan x="32.32" y="0">L</tspan><tspan x="41.58" y="0">O</tspan><tspan x="56.34" y="0">T</tspan><tspan x="67.53" y="0">N</tspan><tspan x="81.95" y="0">I</tspan><tspan x="87.39" y="0">S</tspan><tspan x="97.79" y="0">K</tspan><tspan x="110.57" y="0">U</tspan>
                            </text>
                            <g fill="#fff" clip-path="url(#a)">
                                <path d="M132.24 107.11a9.62 9.62 0 1 1-9.49 9.62 9.57 9.57 0 0 1 9.49-9.62m-28.5 7.55a5 5 0 1 1-5 5 5 5 0 0 1 5-5"/>
                                <path d="M141.74 84.11v-23.3H118.8l-10.07 23.3v10.58h14V84.11h19zm1 8.91h-10.5v3.38h10.54V93zM92 116.74V97.25l20.92-45.77h38.48v65.26h-4.35a14.53 14.53 0 1 0-29.06 0h-7a7.38 7.38 0 0 0-14.65 0H92z"/>
                            </g>
                            <path fill="#fff" d="M87.92 31.13v85.6H57.2v-5.25h25.55V31.13h5.17"/>
                        </svg>

                    </div>

                        <?php if( get_field('s2-title') ): ?>
                            <div class="section-copy">
                                <h1 class="heading"><?php the_field('s2-title') ?><>
                            <p class="copy"><?php the_field('s2-content') ?></p>
                    </div>
                    <?php endif; ?>


				</div>
    <div class="mobile-legend">
        <img src="<?= get_template_directory_uri();?>/dist/images/location-legend-1.jpg" alt="">
    </div>
                <div class="section-content visualization-section">
                    <div class="bg" style="background-image: url('<?= the_field('bg3');?>')"></div>

    <div class="infoboxes-container">


        <img src="<?= get_template_directory_uri();?>/dist/images/icons/location-3-1.svg" data-aos="flip-left" alt="" class="svg-box">
        <img src="<?= get_template_directory_uri();?>/dist/images/icons/location-3-2.svg" data-aos="flip-left" alt="" class="svg-box">
        <img src="<?= get_template_directory_uri();?>/dist/images/icons/location-3-3.svg" data-aos="flip-left" alt="" class="svg-box">
        <img src="<?= get_template_directory_uri();?>/dist/images/icons/location-3-4.svg" data-aos="flip-left" alt="" class="svg-box">
        <img src="<?= get_template_directory_uri();?>/dist/images/icons/location-3-5.svg" data-aos="flip-left" alt="" class="svg-box">
        <img src="<?= get_template_directory_uri();?>/dist/images/icons/location-3-6.svg" data-aos="flip-left" alt="" class="svg-box">
        <img src="<?= get_template_directory_uri();?>/dist/images/icons/location-3-7.svg" data-aos="flip-left" alt="" class="svg-box">
        <img src="<?= get_template_directory_uri();?>/dist/images/icons/location-3-8.svg" data-aos="flip-left" alt="" class="svg-box">

    </div>




    <?php if( get_field('s3-title') ): ?>
        <div class="section-copy">
            <h1 class="heading"><?php the_field('s3-title') ?></h1>
            <p class="copy"><?php the_field('s3-content') ?></p>
        </div>
    <?php endif; ?>





<?php endwhile; ?>