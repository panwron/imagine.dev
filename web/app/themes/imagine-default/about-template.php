<?php
/**
 * Template Name: About project
 */
?>


<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); ?>

				<div class="section-content about-section">
                    <div class="bg" style="background-image: url('<?php the_field("bg1"); ?>')">
                    </div>

					<div class="infoboxes-container">

                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">
                            <path fill="#00b188" d="M0 0h208.63v209.63H0z"/>
                            <text fill="#fff" transform="translate(7.49 172.6)" font-size="21" font-family="Lato" font-weight="700">
                                O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>
                            </text>
                            <path fill="#fff" d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4zm72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78zm-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79z"/>
                            <text fill="#fff" transform="translate(7.44 136.94)" font-size="22" font-family="Lato" font-weight="700">
                                1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" font-size="18">m</tspan>
                            </text>
                            <text fill="#fff" transform="matrix(.58 0 0 .58 83.01 130.94)" font-size="18" font-family="Lato" font-weight="700">
                                2
                            </text>
                            <text fill="#fff" transform="translate(91.65 136.94)" font-size="22" font-family="Lato" font-weight="700">
                                <tspan white-space="pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" font-size="18">m</tspan>
                            </text>
                            <text fill="#fff" transform="matrix(.58 0 0 .58 190.63 130.94)" font-size="18" font-family="Lato" font-weight="700">
                                2
                            </text>
                        </svg>
                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 208.6 208.6">
                            <path fill="#00B188" d="M0 0h208.6v208.6H0z"/>
                            <text transform="translate(10.01 172.14)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Heavy'" font-size="54.713">3</tspan><tspan x="31.4" y="0" fill="#FFF" font-family="'Lato-Heavy'" font-size="54.713">56</tspan>
                            </text>
                            <text transform="translate(110.39 171.46)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">MIEJSC</tspan><tspan x="-100.6" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21" letter-spacing="-1">P</tspan><tspan x="-89" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">A</tspan><tspan x="-74.5" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">R</tspan><tspan x="-61.3" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">K</tspan><tspan x="-47.2" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="-41.2" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="-25" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">G</tspan><tspan x="-9.9" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">O</tspan><tspan x="6.5" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">W</tspan><tspan x="29.2" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21" letter-spacing="-1">Y</tspan><tspan x="41.8" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">C</tspan><tspan x="55.3" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">H</tspan>
                            </text>
                            <g fill="#FFF">
                                <path d="M112 48.3c-2.2 0-3.8.2-4.5.4v14.4c.9.2 2.1.3 3.7.3 5.8 0 9.5-3 9.5-7.9-.1-4.6-3.2-7.2-8.7-7.2"/>
                                <path d="M127 66.7c-3.8 3.5-9.3 5.1-15.8 5.1-1.4 0-2.7-.1-3.8-.2V89H96.5V41c3.4-.6 8.2-1 14.9-1 6.8 0 11.6 1.3 14.9 3.9 3.1 2.5 5.2 6.5 5.2 11.3 0 4.7-1.6 8.7-4.5 11.5m5.7-40.3H87c-7.7 0-14 6.3-14 14v45.7c0 7.7 6.3 14 14 14h45.7c7.7 0 14-6.3 14-14V40.4c0-7.7-6.3-14-14-14"/>
                            </g>
                            <path fill="none" d="M-18.8 0h236.4v208.6H-18.8z"/>
                        </svg>
                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 208.6 208.6">
                            <path fill="#1ABFD5" d="M0 0h208.6v208.6H0z"/>
                            <text fill="#FFF" transform="translate(6.49 126.8)" font-family="'Lato-Heavy'" font-size="36" letter-spacing="-1">
                                12 887
                            </text>
                            <text fill="#FFF" transform="translate(118.92 126.8)" font-family="'Lato-Heavy'" font-size="24">
                                m
                            </text>
                            <text fill="#FFF" transform="matrix(.58 0 0 .58 139.16 118.8)" font-family="'Lato-Heavy'" font-size="24">
                                2
                            </text>
                            <text fill="#FFF" transform="translate(119.43 127.8)" font-family="'Lato-Heavy'" font-size="24">

                            </text>
                            <text transform="translate(7.49 151.85)">
                                <tspan x="0" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">P</tspan><tspan x="12.9" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">O</tspan><tspan x="29.3" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">W</tspan><tspan x="51.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="57.1" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan><tspan x="68.9" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">R</tspan><tspan x="82.6" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Z</tspan><tspan x="94.7" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">C</tspan><tspan x="108.2" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">H</tspan><tspan x="124.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="140.4" y="0" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I </tspan><tspan x="0" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">B</tspan><tspan x="13.6" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="19.7" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">U</tspan><tspan x="35.3" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">R</tspan><tspan x="48.3" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">O</tspan><tspan x="64.7" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">W</tspan><tspan x="86.6" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">E</tspan><tspan x="98.7" y="23" fill="#FFF" font-family="'Lato-Bold'" font-size="21">J</tspan><tspan x="0" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21">(</tspan><tspan x="5.2" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21">2 </tspan><tspan x="22.5" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21">B</tspan><tspan x="36.2" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21">U</tspan><tspan x="51.8" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21" letter-spacing="-1">D</tspan><tspan x="66.7" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21">Y</tspan><tspan x="80.3" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21">N</tspan><tspan x="96.4" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21">K</tspan><tspan x="110.5" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21">I</tspan><tspan x="116.1" y="46" fill="#FFF" font-family="'Lato-Bold'" font-size="21">)</tspan>
                            </text>
                            <path fill="#FFF" d="M146.1 53.9H62.6V34.8c0-2.8 2.2-5.1 5-5.1h24.1v-7.6c0-1.9 1.6-3.5 3.4-3.5h18.3c1.9 0 3.4 1.6 3.4 3.5v7.6h24.1c2.8 0 5 2.3 5 5.1v19.1zm-33.3-27.5c0-1.5-1.1-2.7-2.3-2.7H98.1c-1.3 0-2.3 1.2-2.3 2.7v3.3h17.1v-3.3zM83 59.9h-4.8v4.9H83v-4.9zm60.6 21.9c0 2.7-2.1 4.8-4.7 4.8H69.8c-2.6 0-4.7-2.1-4.7-4.8V56.2h78.5v25.6zm-13.3-21.9h-4.8v4.9h4.8v-4.9z"/>
                            <path fill="none" d="M-17 0h227.5v208.6H-17z"/>
                        </svg>
                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 208.63">
                            <path fill="#77bc1f" d="M0 0h208.63v208.63H0z"/>
                            <path fill="#77bc1f" d="M15.76 0h192.87v208.63H15.76z"/>
                            <text fill="#fff" transform="matrix(.92 0 0 1 21.76 173.83)" font-size="21" font-family="Lato" font-weight="700">
                                P<tspan x="12.49" y="0">O</tspan><tspan x="28.45" y="0">W</tspan><tspan x="49.79" y="0">I</tspan><tspan x="55.39" y="0">E</tspan><tspan x="66.79" y="0">R</tspan><tspan x="80.08" y="0">Z</tspan><tspan x="91.75" y="0">C</tspan><tspan x="104.88" y="0">H</tspan><tspan x="120.6" y="0">N</tspan><tspan x="136.15" y="0">I</tspan><tspan x="141.96" y="0" white-space="pre"> </tspan><tspan x="0" y="23">H</tspan><tspan x="15.63" y="23">A</tspan><tspan x="29.69" y="23">N</tspan><tspan x="45.41" y="23">D</tspan><tspan x="60.94" y="23">L</tspan><tspan x="70.78" y="23">O</tspan><tspan x="86.74" y="23">W</tspan><tspan x="108.24" y="23">E</tspan><tspan x="119.94" y="23">J</tspan>
                            </text>
                            <path fill="#fff" d="M88.92 68.94H146c-.34 1.4-.65 2.73-1 4-1.4 5.58-2.83 11.16-4.2 16.74a1 1 0 0 1-1.25.87H95.2a1 1 0 0 1-1.17-.73c-1.65-6.7-3.33-13.39-5-20.08-.03-.21-.03-.46-.11-.8m-13.4-27.7v-2.88c1.73 0 3.45.08 5.16 0a1.78 1.78 0 0 1 2.17 1.35c1.91 5.3 4 10.56 5.9 15.84a1.42 1.42 0 0 0 1.66 1h58.47c-.65 2.59-1.25 5.06-1.88 7.52-.48 1.88-.5 1.88-2.77 1.88h-54.5c-1.1 0-1.46-.28-1.65-1.18-1.57-7.51-4.9-14.56-7.47-21.82-.6-1.71-.64-1.7-2.74-1.69h-2.35m55.93 53.6a6.45 6.45 0 1 1-6.07 6.38 6.24 6.24 0 0 1 6.07-6.38m-22 6.54a6.19 6.19 0 0 1-6.1 6.32 6.34 6.34 0 0 1-5.87-6.5 6.25 6.25 0 0 1 6.09-6.36 6.18 6.18 0 0 1 5.88 6.54"/>
                            <text fill="#fff" transform="matrix(.92 0 0 1 21.76 146.72)" font-size="36" font-family="Lato">
                                2 268 <tspan x="96.94" y="0" font-size="24">m</tspan>
                            </text>
                            <text fill="#fff" transform="matrix(.54 0 0 .58 130.1 138.73)" font-size="24" font-family="Lato">
                                2
                            </text>
                        </svg>
                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 219.49">
                        <defs>
                            <clipPath id="a">
                                <path fill="none" d="M-30.63 0h239.25v215.26H-30.63z"/>
                            </clipPath>
                        </defs>
                        <text fill="#fff" transform="matrix(.92 0 0 1 21.76 173.83)" font-size="21" font-family="Lato" font-weight="700">
                            P<tspan x="12.49" y="0">O</tspan><tspan x="28.45" y="0">W</tspan><tspan x="49.79" y="0">I</tspan><tspan x="55.39" y="0">E</tspan><tspan x="66.79" y="0">R</tspan><tspan x="80.08" y="0">Z</tspan><tspan x="91.75" y="0">C</tspan><tspan x="104.88" y="0">H</tspan><tspan x="120.6" y="0">N</tspan><tspan x="136.15" y="0">I</tspan><tspan x="141.96" y="0" white-space="pre"> </tspan><tspan x="0" y="23">H</tspan><tspan x="15.63" y="23">A</tspan><tspan x="29.69" y="23">N</tspan><tspan x="45.41" y="23">D</tspan><tspan x="60.94" y="23">L</tspan><tspan x="70.78" y="23">O</tspan><tspan x="86.74" y="23">W</tspan><tspan x="108.24" y="23">E</tspan><tspan x="119.94" y="23">J</tspan>
                        </text>
                        <path fill="#00b188" d="M0 0h208.63v208.63H0z"/>
                        <g clip-path="url(#a)">
                            <text fill="#fff" transform="translate(7.08 196.17)" font-size="80" font-family="Lato">
                                6
                            </text>
                        </g>
                        <path fill="#fff" d="M56.33 113.49h95.7v4.5h-95.7zm0-22.72h95.7v4.5h-95.7zm.27-2.27l9.75-6.25h75.64l9.76 6.25H56.6m0 22.73l9.75-6.24h75.64l9.76 6.24H56.6m0-43.91h95.7v4.5H56.6zm0-22.73h95.7v4.51H56.6zm.27-2.26l9.75-6.25h75.65l9.76 6.25H56.87m0 22.72l9.75-6.23h75.65l9.76 6.23H56.87"/>
                        <text fill="#fff" transform="translate(58.48 196.17)" font-size="24" font-family="Lato" font-weight="700">
                            P
                        </text>
                        <text fill="#fff" transform="translate(72.86 196.17)" font-size="24" font-family="Lato" font-weight="700">
                            I
                        </text>
                        <text fill="#fff" transform="translate(80.15 196.17)" font-size="24" font-family="Lato" font-weight="700">
                            Ę
                        </text>
                        <text fill="#fff" transform="translate(94.08 196.17)" font-size="24" font-family="Lato" font-weight="700">
                            T
                        </text>
                        <text fill="#fff" transform="translate(108.12 196.17)" font-size="24" font-family="Lato" font-weight="700">
                            E
                        </text>
                        <text fill="#fff" transform="translate(121.58 196.17)" font-size="24" font-family="Lato" font-weight="700">
                            R
                        </text>
                    </svg>
                        <svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 216.48 209.21">
                            <text fill="#fff" transform="translate(58.61 196.17)" font-size="24" font-family="Lato" font-weight="700">
                                P
                            </text>
                            <text fill="#fff" transform="translate(72.99 196.17)" font-size="24" font-family="Lato" font-weight="700">
                                I
                            </text>
                            <text fill="#fff" transform="translate(80.28 196.17)" font-size="24" font-family="Lato" font-weight="700">
                                Ę
                            </text>
                            <text fill="#fff" transform="translate(94.2 196.17)" font-size="24" font-family="Lato" font-weight="700">
                                T
                            </text>
                            <text fill="#fff" transform="translate(108.24 196.17)" font-size="24" font-family="Lato" font-weight="700">
                                E
                            </text>
                            <text fill="#fff" transform="translate(121.71 196.17)" font-size="24" font-family="Lato" font-weight="700">
                                R
                            </text>
                            <path fill="#00bfd6" d="M.13 0h208.63v208.63H.13z"/>
                            <path fill="#fff" d="M140.97 114.71a18.15 18.15 0 0 1-18.29-18 18 18 0 0 1 10.62-16.29l5.07 18a2.42 2.42 0 0 0 3 1.65 2.37 2.37 0 0 0 1.68-2.92L137.91 79a18.53 18.53 0 0 1 3.06-.27 18 18 0 1 1 0 35.94m-36.56-23.62a7.6 7.6 0 0 0-3.64-1l-7.64-21.76 32.56-.24zm-1 6.48a2.72 2.72 0 0 1-2.74 2.64 2.75 2.75 0 0 1-1.29-.33 2.68 2.68 0 0 1-1.45-2.36 2.63 2.63 0 0 1 .11-.73 2.66 2.66 0 0 1 .53-1 2.74 2.74 0 0 1 2.1-1h.29a2.74 2.74 0 0 1 1.87 1 2.64 2.64 0 0 1 .58 1.64v.05M93.3 95.69h-2.56a22.52 22.52 0 0 0-8-16.12l6-9.2 7.39 21.13a7.43 7.43 0 0 0-2.87 4.19m-21-.35L80 83.57a17.78 17.78 0 0 1 5.83 12zm-4.66 19.37a18 18 0 1 1 0-35.94 18.42 18.42 0 0 1 8.53 2.05L65.92 96.35a2.34 2.34 0 0 0-.1 2.4A2.42 2.42 0 0 0 67.9 100l17.68.29a18.24 18.24 0 0 1-17.93 14.41M140.97 74a23.56 23.56 0 0 0-4.36.41l-5.06-17.95c8.72-.8 11.61 0 12.57.64a2.75 2.75 0 0 1 .93 2 4.69 4.69 0 0 1-.52 3.08c-.7.76-2.68 1.21-5 1.12a2.372 2.372 0 1 0-.18 4.74c2.25.09 6.38-.15 8.71-2.67a8.79 8.79 0 0 0 1.73-6.87 7.21 7.21 0 0 0-3.09-5.32c-3-1.88-8.68-2.21-18.55-1.07a2.41 2.41 0 0 0-1.72 1 2.34 2.34 0 0 0-.32 2l2.32 8.24-37 .28-1.3-3.97h7.15a2.37 2.37 0 1 0 0-4.74H79.92a2.37 2.37 0 1 0 0 4.74h5.11l1.74 5-8 12.15A23.29 23.29 0 0 0 67.66 74a22.71 22.71 0 1 0 22.81 26.35l3.23.05a7.58 7.58 0 0 0 14.5-3 7.29 7.29 0 0 0-.58-2.85l22.76-24.57 1.62 5.75a23.18 23.18 0 1 0 9-1.78"/>
                            <path fill="none" d="M0 .59h216.48v208.63H0z"/>
                            <text fill="#fff" transform="translate(9.13 171.9)" font-size="21" font-family="Lato" font-weight="700">
                                S<tspan x="11.41" y="0" letter-spacing="-1">T</tspan><tspan x="22.02" y="0">A</tspan><tspan x="36.29" y="0">N</tspan><tspan x="52.22" y="0">O</tspan><tspan x="68.39" y="0">W</tspan><tspan x="89.93" y="0">I</tspan><tspan x="95.76" y="0">S</tspan><tspan x="107.07" y="0">K</tspan><tspan x="121.82" y="0">A</tspan><tspan x="136.21" y="0"> </tspan><tspan x="0" y="23">R</tspan><tspan x="12.79" y="23">O</tspan><tspan x="28.96" y="23">W</tspan><tspan x="50.67" y="23">E</tspan><tspan x="62.26" y="23">R</tspan><tspan x="75.05" y="23">O</tspan><tspan x="91.22" y="23">W</tspan><tspan x="112.94" y="23">E</tspan>
                            </text>
                        </svg>
                        <svg class="svg-box" id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 208.63 209.63"><title>Vector Smart Object12</title><rect width="208.63" height="209.63" fill="#00b188"/><text transform="translate(7.49 172.6)" font-size="21" fill="#fff" font-family="Lato" font-weight="700">O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan></text><rect x="60.82" y="86.25" width="86.9" height="4.31" fill="#fff"/><polygon points="61.07 84.09 69.92 78.11 90.89 78.11 117.64 78.11 138.61 78.11 147.47 84.09 104.51 84.09 104.03 84.09 61.07 84.09" fill="#fff"/><polygon points="133.82 35.69 131.24 38.3 140.47 47.63 117.94 47.63 117.94 51.32 140.47 51.32 131.24 60.64 133.82 63.26 147.47 49.47 133.82 35.69" fill="#fff"/><polygon points="74.72 63.26 77.31 60.64 68.07 51.32 90.6 51.32 90.6 47.63 68.07 47.63 77.31 38.3 74.72 35.69 61.07 49.47 74.72 63.26" fill="#fff"/><text transform="translate(7.44 136.94)" font-size="22" fill="#fff" font-family="Lato" font-weight="700">1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" font-size="18">m</tspan></text><text transform="translate(83.01 130.94) scale(0.58)" font-size="18" fill="#fff" font-family="Lato" font-weight="700">2 </text><text transform="translate(91.65 136.94)" font-size="22" fill="#fff" font-family="Lato" font-weight="700"><tspan style="white-space:pre"> ~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" font-size="18">m</tspan></text><text transform="translate(190.63 130.94) scale(0.58)" font-size="18" fill="#fff" font-family="Lato" font-weight="700">2</text></svg>


                    </div>

                        <?php if( get_field('s1-title') ): ?>
                        <div class="section-copy">
                            <h1 class="heading"><?php the_field('s1-title') ?></h1>
                            <p class="copy"><?php the_field('s1-content') ?></p>
                        </div>
                    <?php endif; ?>
                    






				</div>


    <div class="mobile-legend">
        <img src="<?= get_template_directory_uri();?>/dist/images/about-legend-1.jpg" alt="">
    </div>


				<div class="section-content area-section">
					<div class="legend-panel">
						<h1 class="legend-title">PLAN ZAGOSPODAROWANIA TERENU</h1>
                        <ul class="location-legend">
                            <li>budynek biurowo-usługowy</li>
                            <li>garaż</li>
                            <li>chodniki, jedzenie</li>
                            <li>dziedziniec</li>
                            <li>tereny zielone</li>
                            <li>fontanna</li>
                            <li>granica terenu</li>
                            <li>ścieżka rowerowa</li>
                            <li>wejście główne</li>
                            <li>wejście pomocnicze</li>
                            <li>wejście do usług</li>
                        </ul>

					</div>
					<div class="map-container">

					</div>
				</div>
				<div class="section-content environment-section">
                    <div class="bg" style="background-image: url('<?php the_field('bg2') ?>')">
                    </div>


                    <div class="infoboxes-container">

                        <img src="" alt="" class="svg-box">
                        <img src="<?= get_template_directory_uri();?>/dist/images/icons/about-2-1.svg" alt="" class="svg-box">
                        <img src="<?= get_template_directory_uri();?>/dist/images/icons/about-2-2.svg" alt="" class="svg-box">
                        <img src="" alt="" class="svg-box">

                        <img src="<?= get_template_directory_uri();?>/dist/images/icons/about-2-3.svg" alt="" class="svg-box">
                        <img src="<?= get_template_directory_uri();?>/dist/images/icons/about-2-4.svg" alt="" class="svg-box">

<!--						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">-->
<!--							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>-->
<!--							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>-->
<!--							</text>-->
<!--							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>-->
<!--							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--						</svg>-->
<!--						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">-->
<!--							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>-->
<!--							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>-->
<!--							</text>-->
<!--							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>-->
<!--							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--						</svg>-->
<!--						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">-->
<!--							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>-->
<!--							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>-->
<!--							</text>-->
<!--							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>-->
<!--							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--						</svg>-->
<!--						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">-->
<!--							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>-->
<!--							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>-->
<!--							</text>-->
<!--							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>-->
<!--							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--						</svg>-->
<!--						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">-->
<!--							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>-->
<!--							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>-->
<!--							</text>-->
<!--							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>-->
<!--							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--						</svg>-->
<!--						<svg class="svg-box" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 208.63 209.63">-->
<!--							<path d="M0 0h208.63v209.63H0z" style="fill:#00b188"/>-->
<!--							<text transform="translate(7.49 172.6)" style="font-size:21px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								O<tspan x="16.9" y="0">P</tspan><tspan x="29.88" y="0">T</tspan><tspan x="43.05" y="0">Y</tspan><tspan x="56.62" y="0">M</tspan><tspan x="76.33" y="0">A</tspan><tspan x="90.81" y="0">L</tspan><tspan x="101.43" y="0">N</tspan><tspan x="117.48" y="0">A</tspan><tspan x="0" y="23">W</tspan><tspan x="21.76" y="23">I</tspan><tspan x="27.77" y="23">E</tspan><tspan x="39.62" y="23">L</tspan><tspan x="50.19" y="23">K</tspan><tspan x="63.64" y="23">O</tspan><tspan x="80.44" y="23">Ś</tspan><tspan x="91.73" y="23">Ć </tspan><tspan x="110.71" y="23">P</tspan><tspan x="123.16" y="23">I</tspan><tspan x="129.18" y="23">Ę</tspan><tspan x="141.28" y="23">T</tspan><tspan x="153.62" y="23">R</tspan><tspan x="167.63" y="23">A</tspan>-->
<!--							</text>-->
<!--							<path d="M60.82 86.25h86.9v4.31h-86.9zm.25-2.16l8.85-5.98h68.69l8.86 5.98h-86.4m72.75-48.4l-2.58 2.61 9.23 9.33h-22.53v3.69h22.53l-9.23 9.32 2.58 2.62 13.65-13.79-13.65-13.78m-59.1 27.57l2.59-2.62-9.24-9.32H90.6v-3.69H68.07l9.24-9.33-2.59-2.61-13.65 13.78 13.65 13.79" style="fill:#fff"/>-->
<!--							<text transform="translate(7.44 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								1 <tspan x="17.71" y="0">2</tspan><tspan x="29.63" y="0">30 </tspan><tspan x="60.5" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 83.01 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--							<text transform="translate(91.65 136.94)" style="font-size:22px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								<tspan style="white-space:pre">~</tspan><tspan x="17.71" y="0"> </tspan><tspan x="22.84" y="0">1 </tspan><tspan x="40.54" y="0">5</tspan><tspan x="52.82" y="0">0</tspan><tspan x="65.8" y="0">0 </tspan><tspan x="83.91" y="0" style="font-size:18px">m</tspan>-->
<!--							</text>-->
<!--							<text transform="matrix(.58 0 0 .58 190.63 130.94)" style="font-size:18px;fill:#fff;font-family:Lato;font-weight:700">-->
<!--								2-->
<!--							</text>-->
<!--						</svg>-->


					</div>



                    <?php if( get_field('s2-title') ): ?>
                        <div class="section-copy">
                            <h1 class="heading"><?php the_field('s2-title') ?></h1>
                            <p class="copy"><?php the_field('s2-content') ?></p>
                        </div>
                    <?php endif; ?>

				</div>
    <div class="mobile-legend">
        <img src="<?= get_template_directory_uri();?>/dist/images/about-legend-2.jpg" alt="">
    </div>


<?php endwhile; ?>