<?php //the_content(); ?>
<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

        <div class="col-lg-6 menu-col">
            <h2 class="week-menu-title mgl5">Menu <span>13 - 17 luty 2017</span></h2>


            <!--Pagination -->
            <div class="days-box mgl5">
                <button class="day-selector pagination" data-slide="1">Pn</button>
                <button class="day-selector pagination" data-slide="2">Wt</button>
                <button class="day-selector pagination" data-slide="3">Śr</button>
                <button class="day-selector pagination" data-slide="4">Czw</button>
                <button class="day-selector pagination" data-slide="5">Pt</button>
            </div>
            <!--Pagination  end-->

            <!-- Menu Container -->
            <div class="swiper-container mgl5">

                <div class="swiper-wrapper">



                    <div class="swiper-slide">
                        <div class="weekend">
                            <h1>Niedziela</h1>
                            <p>Zapraszamy ponownie już jutro</p>
                        </div>
                    </div>
                    <div class="swiper-slide">

                        <div class="day-menu">
                        <h3>Poniedziałek</h3>

                                    <ul class="menu">
                                        <li><h4 class="course">Zupa brokułowa<span class="price" >3.50 zł</span></h4>
                                        </li>
                                        <li><h4 class="course">
                                                Kotlet drobiowy w sezamowej <br>
                                                panierce
                                                 <span class="price">14.00</span>
                                            </h4>
                                            <p class="sides">
                                                pieczone ziemniaki, zestaw surówek
                                            </p>
                                        </li>
                                        <li><h4 class="course">Boeuf stroganow <span class="price">13.00 zł</span></h4>
                                            <p class="sides">kluski śląskie, zestaw surówek</p>
                                        </li>
                                        <li>
                                            <h4 class="course">Kotlet mielony z łopatki <br> wieprzowej<span class="price">12.00 zł</span>
                                            </h4>
                                            <p class="sides">
                                           puree ziemniaczane, zestaw surówek
                                            </p>

                                        </li>
                                        <li>
                                            <h4 class="course">Fasolka szparagowa<span class="price">10.50 zł</span></h4>
                                                <p class="sides">
                                                ziemniaki, jajko sadzone, prażona bułka tarta</p>
                                        </li>
                                    </ul>

                        </div>

                    </div>
                    <div class="swiper-slide">

                        <div class="day-menu">
                            <h3>Wtorek</h3>

                            <ul class="menu">
                            <li><h4 class="course">Zupa pomidorowa<span class="price" >3.50 zł</span></h4>
                
                                </li>
                                <li><h4 class="course">Krem cebulowy<span class="price" >5.00 zł</span></h4>
                                  <p class="sides">
                                        grzanki ziołowe
                                    </p>
                                </li>
                                <li><h4 class="course">
                                        Kotlet po kijowsku z masłem ziołowym <span class="price">
                                    14.00
                                </span>
                                    </h4>
                                    <p class="sides">
                                        puree ziemniaczane, zestaw surówek
                                    </p>
                                </li>
                                <li><h4 class="course">Spaghetti z sosem bolońskim <span class="price">12.00 zł</span></h4>
                                </li>
                                <li>
                                    <h4 class="course">Tradycyjne duszonki<span class="price"> 11.00 zł</span>
                                    </h4>


                                </li>
                                <li>
                                    <h4 class="course">Naleśniki ze szpinakiem i riccottą <span class="price">11.00 zł</span></h4>

                                </li>
                            </ul>

                        </div>

                    </div>
                    <div class="swiper-slide">

                        <div class="day-menu">
                            <h3>Środa</h3>

                            <ul class="menu">
                                <li><h4 class="course">Kapuśniak<span class="price" >4.00 zł</span></h4>
                                </li>
                                <li><h4 class="course">Zupa pomidorowa<span class="price" >3.50 zł</span></h4>
                                </li>
                                <li><h4 class="course">
                                        Schab zapiekany z pieczarkami i <br> serem<span class="price">
                                    14.00 zł
                                </span>
                                    </h4>
                                    <p class="sides">
                                        puree ziemniaczane, zestaw surówek
                                    </p>
                                </li>
                                <li><h4 class="course">Kurczak z warzywami w sosie <br> słodko pikantnym <span class="price">11.00 zł</span></h4>
                                </li>
                                <li>
                                    <h4 class="course">Bigos <span class="price"> 10.00 zł</span>
                                    </h4>
                                    <p class="sides"> pieczywo lub puree ziemniaczane</p>
                                </li>
                                <li>
                                    <h4 class="course" style="">Frittata warzywna<span class="price">10.00 zł</span></h4>
                            

                                </li>
                            </ul>

                        </div>

                    </div>
                    <div class="swiper-slide">

                        <div class="day-menu">
                        <h3>Czwartek</h3>

                            <ul class="menu">
                                <li><h4 class="course">Zupa krem z dyni <span class="price" >5.00 zł</span></h4>
                                              <p class="sides">
                                     granki
                                    </p>
                                </li>
                                <li><h4 class="course">Kapuśniak <span class="price" >3.50 zł</span></h4>
                             
                                </li>

                                <li><h4 class="course">
                                        Filet z kurczaka zapiekany z mozzarellą, pomidorami i <br> świeżą bazylią <span class="price">
                                    14.50
                                </span>
                                    </h4>
                                    <p class="sides">
                                        puree ziemniaczane, zestaw surówek
                                    </p>
                                </li>
                                <li><h4 class="course">Zapiekanka makaronowa z klopsikami <br> z indyka
                                        <span class="price">12.00 zł</span></h4>
                                </li>
                                <li>
                                    <h4 class="course">Flaczki wołowe po warszawsku<span class="price"> 8.00 zł</span>
                                    </h4>
                                          <p class="sides">
                                        pieczywo
                                    </p>

                                </li>
                                <li>
                                    <h4 class="course">Kalafior panierowany
		
                                        <span class="price">10.00 zł</span></h4>
                                                <p class="sides">
                                        puree ziemniaczane, sos, surówka
                                    </p>
                                </li>
                            </ul>

                        </div>

                    </div>
                    <div class="swiper-slide">

                        <div class="day-menu">
                            <h3>Piątek</h3>


                            <ul class="menu">
                                <li><h4 class="course">Zupa ogórkowa z ryżem<span class="price" >3.50 zł</span></h4>
                                </li>
                                <li><h4 class="course">
                                        Panierowany filet z dorsza <span class="price">
                                    14.00
                                </span>
                                    </h4>
                                    <p class="sides">
                                        frytki, zestaw surówek
                                    </p>
                                </li>
                                <li><h4 class="course">Roladka z limandy w sosie <br> koperkowo-cytrynowym <span class="price">13.50 zł</span></h4>
                                    <p class="sides"> puree ziemniaczane, zestaw surówek</p>
                                </li>
                                <li>
                                    <h4 class="course">Karczek z grilla z cebulą<span class="price"> 13.00 zł</span>
                                    </h4>
                                    <p class="sides"> puree ziemniaczane, zestaw surówek</p>

                                </li>
                                <li>
                                    <h4 class="course">Łazanki wegetariańskie z pieczarkami i serem <span class="price">10.00 zł</span></h4>

                                </li>
                            </ul>

                        </div>

                    </div>
                    <div class="swiper-slide">
                        <div class="weekend">
                            <h1>Sobota</h1>
                            <p>Zapraszamy ponownie w poniedziałek</p>
                        </div>
                    </div>


                </div>
            </div>
            <!-- Menu Container  End-->

        </div>
        <!--MENU -->


        <div class="col-lg-6 content-col">
            <!-- Brand info -->

            <div class="order">
                <h1>W BB Lunch - <span class="orange" >obiady Bielsko-Biała</span></h1>
                <p>
                    stawiamy przede wszystkim na smaczną kuchnie. Codziennie ze <span class="green">świeżych składników</span>  przygotowujemy <span>kartę dnia</span>. Zaczynamy od tradycyjnych przepisów i smaków określanych jako <span>domowe</span>, nie zabraknie jednak <span>nowoczesnych akcentów</span>, od czasu do czasu będziemy też <span class="red">eksperymentować</span>  żeby z naszej kuchni nie pachniało nudą. Najpierw jednak chcemy się <span>poznać</span>, dlatego serdecznie zapraszmy do wypróbowania naszych dań.
                </p>
                <hr class="content-hr">
            </div>

            <!-- Brand info end-->


            <!--Order-info-->
            <div class="order grande">
<!--            <div class="info">-->
<!--                <svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 496.3 496.3"><path d="M248.1 0C111.3 0 0 111.3 0 248.2c0 136.8 111.3 248.2 248.1 248.2 136.8 0 248.2-111.3 248.2-248.2C496.3 111.3 385 0 248.1 0zM248.1 472.1c-123.5 0-223.9-100.5-223.9-223.9 0-123.5 100.5-223.9 223.9-223.9 123.5 0 223.9 100.5 223.9 223.9C472.1 371.6 371.6 472.1 248.1 472.1zM319.5 383.4v32.9c0 1.4-1.1 2.5-2.5 2.5H196.5c-1.4 0-2.5-1.1-2.5-2.5V383.4c0-1.4 1.1-2.5 2.5-2.5h34.7V205.8h-35.1c-1.4 0-2.5-1.1-2.5-2.5v-33.6c0-1.4 1.1-2.5 2.5-2.5h82.3c1.4 0 2.5 1.1 2.5 2.5v211.2h36.2C318.4 380.9 319.5 382 319.5 383.4zM209.9 105.9c0-20.9 16.9-37.8 37.8-37.8 20.9 0 37.8 16.9 37.8 37.8s-16.9 37.8-37.8 37.8C226.9 143.8 209.9 126.8 209.9 105.9z" fill="#e8bd10"/></svg>-->
<!--            </div>-->
                <p>Zamówienia przyjmujemy:</p>
                <p>od poniedziłku do piątku <span>10.00 - 15.00</span></p>
                <p>Dostawy realizujemy między <span>11.00 a 16.00</span></p>
<!--                <p>Minimalna kwota zamówienia <span>30zł</span></p>-->
                <p>Zestaw Zupa + II danie zawsze <span>Rabat 1.00 zł</span></p>
                <p>Jeszcze przez jakiś czas płatności realizujemy tylko <span>gotówką</span> </p>
                <hr class="content-hr">
            </div>
            <!--Order-info end-->

<!--            <div class="order">-->
<!--                <h1>Aktualności</h1>-->
<!--                <p>-->
<!--                    stawiamy przede wszystkim na smaczną kuchnie. Codziennie ze <span class="green">świeżych składników</span>  przygotowujemy <span>kartę dnia</span>. Zaczynamy od tradycyjnych przepisów i smaków określanych jako <span>domowe</span>, nie zabraknie jednak <span>nowoczesnych akcentów</span>, od czasu do czasu będziemy też <span class="red">eksperymentować</span>  żeby z naszej kuchni nie pachniało nudą. Najpierw jednak chcemy się <span>poznać</span>, dlatego serdecznie zapraszmy do wypróbowania naszych dań.-->
<!--                </p>-->
<!--                <hr class="content-hr">-->
<!--            </div>-->

        </div>
        <!--Content-col end -->